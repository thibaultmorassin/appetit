import axios from "axios";

const burl = process.env.REACT_APP_BACK_DOMAIN;
const headers = {
    "Content-Type": "application/json",
};
// eslint-disable-next-line
export default {
    loginUser: function (pseudo) {
        return axios.get(
            `${burl}/users/loginUser`,
            {
                params: {
                    pseudo: pseudo
                },
                headers: {
                    headers
                }
            });
    },
}
