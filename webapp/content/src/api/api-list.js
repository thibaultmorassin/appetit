import axios from "axios";

const burl = process.env.REACT_APP_BACK_DOMAIN;
const headers = {
    "Content-Type": "application/json",
};

export default {
    newList: function (list) {
        return axios.post(
            `${burl}/list/newList`,
            {
                params: {
                    list: list
                },
                headers: {
                    headers
                }
            });
    },

    getList: function () {
        return axios.get(
            `${burl}/list/getList`,
            {
                headers: {
                    headers
                }
            });
    },

    filterList: function (type) {
        return axios.get(
            `${burl}/list/filterList`,
            {
                params: {
                    type: type
                },
                headers: {
                    headers
                }
            });
    },

    deleteItem: function (itemId) {
        return axios.get(
            `${burl}/list/deleteItem`,
            {
                params: {
                    itemId: itemId
                },
                headers: {
                    headers
                }
            });
    },

    deleteList: function () {
        return axios.get(
            `${burl}/list/deleteList`,
            {
                headers: {
                    headers
                }
            });
    },
}
