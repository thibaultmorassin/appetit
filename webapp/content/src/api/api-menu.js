import axios from "axios";

const burl = process.env.REACT_APP_BACK_DOMAIN;
const headers = {
    "Content-Type": "application/json",
};

export default {
    newMenu: function (menu) {
        return axios.post(
            `${burl}/menu/newMenu`,
            {
                params: {
                    menu: menu
                },
                headers: {
                    headers
                }
            });
    },

    updateMenu: function (menu) {
        return axios.post(
            `${burl}/menu/updateMenu`,
            {
                params: {
                    menu: menu
                },
                headers: {
                    headers
                }
            });
    },

    getTodayMenu: function () {
        return axios.get(
            `${burl}/menu/getTodayMenu`,
            {
                headers: {
                    headers
                }
            });
    },

    getWeekMenu: function () {
        return axios.get(
            `${burl}/menu/getWeekMenu`,
            {
                headers: {
                    headers
                }
            });
    },

    deleteMenu: function (day) {
        return axios.post(
            `${burl}/menu/deleteMenu`,
            {
                params: {
                    day: day
                },
                headers: {
                    headers
                }
            });
    },
}
