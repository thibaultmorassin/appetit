import axios from "axios";

const burl = process.env.REACT_APP_BACK_DOMAIN;
const headers = {
    "Content-Type": "application/json",
};

export default {
    newRecipe: function (recipe) {
        return axios.post(
            `${burl}/recipe/newRecipe`,
            {
                params: {
                    recipe: recipe
                },
                headers: {
                    headers
                }
            });
    },

    changeFavorisRecipe: function (recipe) {
        return axios.post(
            `${burl}/recipe/changeFavorisRecipe`,
            {
                params: {
                    recipe: recipe
                },
                headers: {
                    headers
                }
            });
    },

    deleteRecipe: function (recipe) {
        return axios.post(
            `${burl}/recipe/deleteRecipe`,
            {
                params: {
                    recipe: recipe
                },
                headers: {
                    headers
                }
            });
    },

    getFilteredRecipe: function (category, filters) {
        return axios.get(
            `${burl}/recipe/getFilteredRecipe`,
            {
                params: {
                   category: category,
                    filters: filters
                },
                headers: {
                    headers
                }
            });
    },

    getFavorisRecipe: function (category, filters) {
        return axios.get(
            `${burl}/recipe/getFavorisRecipe`,
            {
                params: {
                    category: category,
                    filters: filters
                },
                headers: {
                    headers
                }
            });
    },

    getRecipeById: function (recipe) {
        return axios.post(
            `${burl}/recipe/getRecipeById`,
            {
                params: {
                    recipe: recipe
                },
                headers: {
                    headers
                }
            });
    },
}
