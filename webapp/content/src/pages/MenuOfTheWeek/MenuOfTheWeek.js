import React, {useState, useEffect, useContext} from "react";
import './MenuOfTheWeek.css';
import {Row, Col, Container} from "react-bootstrap";
import Footer from "../../components/Footer/Footer";
import NavbarAppetit from "../../components/NavbarAppetit/NavbarAppetit";
import PictureBand from "../../components/PictureBand/PictureBand";
import menuBackground from "../../images/menu-background.png";
import Button from "../../components/Button/Button";
import GlobalState from "../../context/GlobalState";
import MenuDayCard from "../../components/MenuDayCard/MenuDayCard";
import {getThisWeeksDates} from "../../functions/date-function";
import arrowReset from "../../images/icons/rotate-ccw.svg";
import * as Icon from 'react-feather';
import apiMenu from "../../api/api-menu";
import {cryptData, decryptData} from "../../functions/users-infos";
import Popup from "../../components/Popup/Popup";
import cross from "../../images/icons/x.svg";
import check from "../../images/icons/check.svg";
import {Helmet} from "react-helmet";

export default function MenuOfTheWeek() {
    const globalState = useContext(GlobalState);
    const [deleteWholeMenuPopup, setDeleteWholeMenuPopup] = useState(false);
    const [deletingMenuLoader, setDeletingMenuLoader] = useState(false);
    const [menuDisplay, setMenuDisplay] = useState("list");
    const [weekMenu, setWeekMenu] = useState({});

    useEffect(() => {
        apiMenu.getWeekMenu().then(result => {
            if (result.status === 200) {
                setWeekMenu(decryptData(result.data.weekMenu).findRecipe);
            }
        })
    }, []);

    const deleteMenu = (day) => {
        setDeletingMenuLoader(true);
        if (weekMenu.length)
            apiMenu.deleteMenu(cryptData(day)).then(result => {
                if (result.status === 200) {
                    setWeekMenu(decryptData(result.data.weekMenu).findRecipe);
                    setDeleteWholeMenuPopup(false);
                    setDeletingMenuLoader(false);
                }
            });
        else {
            setDeleteWholeMenuPopup(false);
            setDeletingMenuLoader(false);
        }
    }

    const getRecipesOfTheDay = (dayIndex) => {
        var recipesOfTheDay = [];
        if (weekMenu !== {}) {
            Object.keys(weekMenu).map((menu, index) => {
                if (weekMenu[menu].day === dayIndex)
                    recipesOfTheDay = [...recipesOfTheDay, weekMenu[menu]];
            })
        }
        return recipesOfTheDay;
    };

    const renderMenuDayCards = () => {
        const menuCardList = [];
        const today = new Date().getDay();
        const currentWeek = getThisWeeksDates(today);
        [1, 2, 3, 4, 5, 6, 0].map((i, index) => {
            menuCardList.push(<MenuDayCard activeDay={today === i}
                                           day={i}
                                           key={i}
                                           date={currentWeek[i]}
                                           menuDisplay={menuDisplay}
                                           recipesOfTheDay={getRecipesOfTheDay(i)}
                                           handleWeekMenu={setWeekMenu}
                                           handleOnClick={() => deleteMenu(i)}/>)
        })
        return menuCardList;
    }

    const deleteWholeMenuPopupContent =
        <div className="px-3">
            <h5 className="text-center font-weight-bold">Réinitialiser le menu</h5>
            <hr/>
            <p className="text-left">Êtes-vous certain de vouloir réinialiser le menu ?</p>
            <Row className="mx-3">
                <Col sm={6} className="p-0 mb-3 text-center w-100">
                    <Button title={deletingMenuLoader ? "Suppression..." : "Réinitialiser"}
                            iconRight={check}
                            variant={"black"}
                            width={globalState.isMobile ? "100%" : ""}
                            handleOnClick={() => deleteMenu("")}
                    />
                </Col>
                <Col sm={6} className="p-0 text-center w-100">
                    <Button title={"Annuler"}
                            iconRight={cross}
                            width={globalState.isMobile ? "100%" : ""}
                            variant={"secondary"}
                            handleOnClick={() => setDeleteWholeMenuPopup(false)}
                    />
                </Col>
            </Row>
        </div>;

    return (
        <>
            {
                globalState.isMobile &&
                <div className="btn-added-to-navbar">
                    <Button title={"Réinitialiser"}
                            iconRight={arrowReset}
                            width={"100%"}
                            variant={"primary"}
                            handleOnClick={() => setDeleteWholeMenuPopup(true)}
                    />
                </div>
            }
            <Helmet>
                <title>APPETIT - Menu de la semaine</title>
            </Helmet>
            <div className="apt-page">
                {
                    deleteWholeMenuPopup &&
                    <Popup onClose={() => setDeleteWholeMenuPopup(false)}>
                        <div className="popup-add-recipe">
                            {deleteWholeMenuPopupContent}
                        </div>
                    </Popup>
                }
                <NavbarAppetit/>
                {
                    !globalState.isMobile &&
                    <PictureBand backgroundPicture={menuBackground} height={'35vh'}>
                        <h1 className="page-title">AU MENU CETTE SEMAINE</h1>
                        <hr className="page-subline"/>
                    </PictureBand>
                }
                <Container>
                    <div className={globalState.isMobile ? "py-3" : "apt-container my-3"}>
                        <h5 className={globalState.isMobile ? "text-uppercase font-weight-bold text-center py-3" : "apt-container-title mb-2 mt-1"}>
                            Le menu de la semaine
                        </h5>
                        {
                            globalState.isMobile &&
                            <div className="menu-switch">
                                <Icon.List
                                    className={"ml-auto mr-1" + (menuDisplay === "list" ? " bg-dark" : "")}
                                    color={menuDisplay === "list" ? "#fafafa" : "#1e1e1b"}
                                    onClick={() => setMenuDisplay("list")}/>
                                <Icon.Grid className={"mx-1" + (menuDisplay === "grid" ? " bg-dark" : "")}
                                           color={menuDisplay === "grid" ? "#fafafa" : "#1e1e1b"}
                                           onClick={() => setMenuDisplay("grid")}/>
                            </div>
                        }
                        <Row className={menuDisplay === "list" ? "" : "px-2"}>
                            {renderMenuDayCards()}
                            {
                                !globalState.isMobile &&
                                <Col md={3}>
                                    <div className="text-center d-flex align-items-center h-100">
                                        <div className="text-center w-100">
                                            <Button title={"Réinitialiser"}
                                                    iconRight={arrowReset}
                                                    variant={"primary"}
                                                    handleOnClick={() => setDeleteWholeMenuPopup(true)}
                                            />
                                        </div>
                                    </div>
                                </Col>
                            }
                        </Row>
                    </div>
                </Container>
                <Footer/>
            </div>
        </>
    );
}

