import React, {useContext, useEffect, useState} from "react";
import './RecipePage.css';
import {Row, Col, Container} from "react-bootstrap";
import NavbarAppetit from "../../components/NavbarAppetit/NavbarAppetit";
import Footer from "../../components/Footer/Footer";
import menuIcon from "../../images/icons/more-horizontal.svg";
import fake from "../../images/recipe.png";
import check from "../../images/icons/check.svg";
import cross from "../../images/icons/x.svg";
import RecipeIconInfos from "../../components/Recipe/RecipeIconInfos/RecipeIconInfos";
import CategoryFilter from "../../components/CategoryFilter/CategoryFilter";
import TagFilter from "../../components/TagFilter/TagFilter";
import IngredientList from "../../components/IngredientList/IngredientList";
import GlobalState from "../../context/GlobalState";
import Button from "../../components/Button/Button";
import plus from "../../images/icons/plus.svg";
import heartFilled from "../../images/icons/heart-filled.svg";
import heart from "../../images/icons/heart.svg";
import RecipeSteps from "../../components/RecipeSteps/RecipeSteps";
import apiRecipe from "../../api/api-recipe";
import apiMenu from "../../api/api-menu";
import {cryptData, decryptData} from "../../functions/users-infos";
import Popup from "../../components/Popup/Popup";
import SelectGroupItem from "../../components/SelectGroupItem/SelectGroupItem";
import SelectGroup from "../../components/SelectGroup/SelectGroup";
import GuestNumberPicker from "../../components/GuestNumberPicker/GuestNumberPicker";
import FullPageLoader from "../../components/FullPageLoader/FullPageLoader";
import {Helmet} from "react-helmet";
import InputCheckBox from "../../components/InputCheckBox/InputCheckBox";
import apiList from "../../api/api-list";
import Notification from "../../components/Toast/Notification";

export default function RecipePage() {
    const globalState = useContext(GlobalState);

    const [isRecipeLoading, setIsRecipeLoading] = useState(true);

    const [showDropdownMenu, setShowDropdownMenu] = useState(false);
    const [showDeletePopup, setShowDeletePopup] = useState(false);

    const [showMenuSuccessNotification, setShowMenuSuccessNotification] = useState(false);
    const [showListSuccessNotification, setShowListSuccessNotification] = useState(false);

    const [recipe, setRecipe] = useState({})
    const [addRecipeDay, setAddRecipeDay] = useState("");
    const [addRecipeMoment, setAddRecipeMoment] = useState("");
    const [addRecipeGuestNumber, setAddRecipeGuestNumber] = useState(2);
    const [generateIngredientList, setGenerateIngredientList] = useState(true);

    const [addRecipeMenuPopup, setAddRecipeMenuPopup] = useState(false);

    const getRecipe = () => {
        apiRecipe.getRecipeById(localStorage.getItem("recipe")).then(result => {
            if (result.status === 200) {
                setRecipe(decryptData(result.data.recipe).findRecipe);
                setIsRecipeLoading(false);
            }
        })
    }

    const updateRecipe = () => {
        window.location = "/modifier-une-recette/" + recipe.title;
    }

    const changeFavoris = () => {
        if (localStorage.getItem("recipe") !== null) {
            apiRecipe.changeFavorisRecipe(localStorage.getItem("recipe")).then(result => {
                if (result.status === 200) {
                    setRecipe(decryptData(result.data.recipe).findRecipe);
                }
            })
        }
    }

    const deleteRecipe = () => {
        if (localStorage.getItem("recipe") !== null) {
            apiRecipe.deleteRecipe(localStorage.getItem("recipe")).then(result => {
                if (result.status === 200) {
                    window.location = "/recettes";
                }
            })
        }
    }

    useEffect(() => {
        if (localStorage.getItem("recipe") !== null)
            getRecipe();
    }, []);

    const addMenu = () => {
        if (addRecipeDay !== "" && addRecipeGuestNumber !== "" && addRecipeMoment) {
            let menu = {
                day: addRecipeDay,
                peopleNumber: addRecipeGuestNumber,
                moment: addRecipeMoment,
                recipeId: recipe._id,
            }
            apiMenu.newMenu(cryptData(menu)).then(result => {
                if (result.status === 200) {
                    setAddRecipeMenuPopup(false);
                    setShowMenuSuccessNotification(true);
                }
            });
        }
        if (generateIngredientList) {
            recipe.ingredients.map((ingredient) => {
                let newItem = {
                    quantity: ingredient.quantity * addRecipeGuestNumber,
                    unity: ingredient.unity,
                    label: ingredient.label,
                    type: "generated",
                }
                apiList.newList(cryptData(newItem));
                setShowListSuccessNotification(true);
            })
        }
        setAddRecipeMenuPopup(false);
    }

    const recipeActionButtons =
        <div className="mb-4 text-center">
            <div className="mb-3">
                <Button title={"ajouter au menu"}
                        iconLeft={plus}
                        width={"100%"}
                        variant={"primary"}
                        handleOnClick={() => setAddRecipeMenuPopup(true)}
                />
            </div>
            <div>
                <Button title={recipe.favoris ? "Recette favorite" : "Favoris"}
                        iconLeft={recipe.favoris ? heartFilled : heart}
                        width={"100%"}
                        variant={recipe.favoris ? "secondary" : "primary"}
                        handleOnClick={() => changeFavoris()}
                />
            </div>
        </div>;

    const popupDelete =
        <>
            <h4 className="mb-3">Supprimer la recette</h4>
            <p>Êtes vous sûr(e) de vouloir supprimer {recipe.title} ?</p>
            <div className="d-flex">
                <div className="text-center w-100">
                    <Button title={"Supprimer"}
                            iconLeft={check}
                            variant={"primary"}
                            handleOnClick={() => deleteRecipe(recipe)}
                    />
                </div>
                <div className="text-center w-100">
                    <Button title={"Annuler"}
                            iconLeft={cross}
                            variant={"secondary"}
                            handleOnClick={() => setShowDeletePopup(false)}
                    />
                </div>
            </div>
        </>

    const popupAddRecipeMenuContent =
        <div className="popup-edit-menu">
            <h5 className="text-center font-weight-bold">Ajouter la recette au menu :</h5>
            <h5 className="text-center mb-3">{recipe.title}</h5>
            <hr/>
            <SelectGroup label={"Jour :"} value={addRecipeDay}>
                <div className="popup-add-recipe-to-menu-days apt-scroll apt-scroll-hidden">

                    <SelectGroupItem label={"Lundi"}
                                     className="mx-2 px-3"
                                     handleOnClick={() => setAddRecipeDay(1)}
                                     isSelected={addRecipeDay === 1}/>
                    <SelectGroupItem label={"Mardi"}
                                     className="mx-2 px-3"
                                     handleOnClick={() => setAddRecipeDay(2)}
                                     isSelected={addRecipeDay === 2}/>
                    <SelectGroupItem label={"Mercredi"}
                                     className="mx-2 px-3"
                                     handleOnClick={() => setAddRecipeDay(3)}
                                     isSelected={addRecipeDay === 3}/>
                    <SelectGroupItem label={"Jeudi"}
                                     className="mx-2 px-3"
                                     handleOnClick={() => setAddRecipeDay(4)}
                                     isSelected={addRecipeDay === 4}/>
                    <SelectGroupItem label={"Vendredi"}
                                     className="mx-2 px-3"
                                     handleOnClick={() => setAddRecipeDay(5)}
                                     isSelected={addRecipeDay === 5}/>
                    <SelectGroupItem label={"Samedi"}
                                     className="mx-2 px-3"
                                     handleOnClick={() => setAddRecipeDay(6)}
                                     isSelected={addRecipeDay === 6}/>
                    <SelectGroupItem label={"Dimanche"}
                                     className="mx-2 px-3"
                                     handleOnClick={() => setAddRecipeDay(0)}
                                     isSelected={addRecipeDay === 0}/>
                </div>
            </SelectGroup>
            <hr/>
            <SelectGroup label={"Moment :"} value={addRecipeMoment} centerItems={true}>
                <SelectGroupItem label={"MIDI"}
                                 className="mx-2 px-5"
                                 handleOnClick={() => setAddRecipeMoment("MIDI")}
                                 isSelected={addRecipeMoment === "MIDI"}/>
                <SelectGroupItem label={"SOIR"}
                                 className="mx-2 px-5"
                                 handleOnClick={() => setAddRecipeMoment("SOIR")}
                                 isSelected={addRecipeMoment === "SOIR"}/>
            </SelectGroup>
            <div className="mt-3">
                <p>Nombre de personnes :</p>
                <div className="position-relative w-75 m-auto d-flex">
                    <p className="guest-number-label mx-5">
                        <span
                            className="font-weight-bold">{addRecipeGuestNumber}</span> personne{addRecipeGuestNumber === 1 ? "" : "s"}
                    </p>
                    <GuestNumberPicker guestNumber={addRecipeGuestNumber} setGuestNumber={setAddRecipeGuestNumber}/>
                </div>
            </div>
            <div className="mt-3">
                <InputCheckBox controlId={"input-generate-ingredient-list"}
                               label={"Générer la liste de courses"}
                               value={generateIngredientList}
                               handleOnChange={setGenerateIngredientList}/>
            </div>
        </div>

    return (
        <>
            {
                !isRecipeLoading &&
                <Helmet>
                    <title>APPETIT - {recipe.title}</title>
                </Helmet>
            }
            <div className="apt-notification-container">
                {
                    showMenuSuccessNotification &&
                    <Notification
                        title={"Succès"}
                        content={"La recette a correctement été ajoutée au menu."}
                        show={showMenuSuccessNotification}
                        handleOnClose={() => setShowMenuSuccessNotification(false)}
                        time={"Maintenant"}/>
                }
                {
                    showListSuccessNotification &&
                    <Notification
                        title={"Succès"}
                        content={"La liste de courses a bien été générée."}
                        show={showListSuccessNotification}
                        handleOnClose={() => setShowListSuccessNotification(false)}
                        time={"Maintenant"}/>
                }
            </div>
            <div className={"apt-page" + (globalState.isMobile ? " bg-white" : "")}>
                <NavbarAppetit/>
                {
                    isRecipeLoading &&
                    <div className="position-relative">
                        <FullPageLoader/>
                    </div>
                }
                <Container>
                    <div
                        className={globalState.isMobile ? "mt-5 position-relative" : "apt-container mt-4 position-relative"}>
                        {
                            !globalState.isMobile &&
                            <h2 className="recipe-title">{recipe.title}</h2>
                        }
                        <div className="recipe-menu"
                             onClick={() => setShowDropdownMenu(!showDropdownMenu)}>
                            <img className={"recipe-menu-icon" + (showDropdownMenu ? " icon-animate" : "")}
                                 src={menuIcon} alt={"•••"}
                                 draggable={false}/>
                            {
                                showDropdownMenu &&
                                <>
                                    <div className="recipe-dropdown-triangle"/>
                                    <div className="recipe-dropdown-menu">
                                        <div className="recipe-dropdown-item" onClick={() => updateRecipe()}>
                                            Modifier
                                        </div>
                                        <hr className="recipe-dropdown-menu-separator"/>
                                        <div className="recipe-dropdown-item" onClick={() => setShowDeletePopup(true)}>
                                            Supprimer
                                        </div>
                                    </div>
                                </>
                            }
                        </div>
                        <Row>
                            <Col md={4}>
                                <img className="recipe-image" src={recipe.picture ? recipe.picture : fake} alt={"•••"}
                                     draggable={false}/>
                            </Col>
                            <Col md={5}>
                                {
                                    globalState.isMobile &&
                                    <h2 className="recipe-title">{recipe.title}</h2>
                                }
                                <RecipeIconInfos recipe={recipe} recipePage={true}/>
                                <div className="recipe-filters apt-scroll apt-scroll-hidden">
                                    <CategoryFilter title={recipe.category} active={true} disabled={true}/>
                                    {
                                        recipe.tag !== undefined &&
                                        recipe.tag.map((tag, index) => {
                                                return <TagFilter title={tag} active={true} disabled={true} key={index}/>
                                            }
                                        )
                                    }
                                </div>
                                {
                                    globalState.isMobile &&
                                    recipeActionButtons
                                }
                                <IngredientList ingredients={recipe.ingredients}/>
                                <RecipeSteps steps={recipe.step}/>
                            </Col>
                            {
                                !globalState.isMobile &&
                                <Col md={3}>
                                    {recipeActionButtons}
                                </Col>
                            }
                        </Row>
                        {
                            showDeletePopup &&
                            <Popup onClose={() => setShowDeletePopup(false)}>
                                <div className="popup-add-recipe">
                                    {popupDelete}
                                </div>
                            </Popup>
                        }
                    </div>
                    {
                        addRecipeMenuPopup &&
                        <Popup closeButton={true} closeButtonText={"Valider"} onClose={() => addMenu()}>
                            <div className="popup-add-recipe-to-menu">
                                {popupAddRecipeMenuContent}
                            </div>
                        </Popup>
                    }
                </Container>
                <Footer/>
            </div>
        </>
    );
}