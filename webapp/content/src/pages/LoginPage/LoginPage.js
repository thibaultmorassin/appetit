import React, {useState, useEffect} from "react";
import './LoginPage.css';
import {Row, Col} from "react-bootstrap";
import InputGroup from "../../components/InputGroup/InputGroup";
import InputCheckBox from "../../components/InputCheckBox/InputCheckBox";
import Button from "../../components/Button/Button";
import arrowRight from "../../images/icons/arrow-right.svg";
import alert from "../../images/icons/alert.svg";
import {useAuth} from "../../context/authentificationContext";
import PulseLoader from "react-spinners/PulseLoader";
import api from "../../api/api-users";


import {retrievedFromJwt} from "../../functions/users-infos";
import {Helmet} from "react-helmet";

let bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

export default function LoginPage() {
    const [pseudoValue, setPseudoValue] = useState("");
    const [passwordValue, setPasswordValue] = useState("");
    const [rememberMe, setRememberMe] = useState(true);
    const [emptyFieldAlert, setEmptyFieldAlert] = useState(false);
    const [wrongAccountAlert, setWrongAccountAlert] = useState(false);
    const [connexionIssueAlert, setConnexionIssueAlert] = useState(false);

    const [isLoggedIn, setLoggedIn] = useState(!!localStorage.getItem("tokens"));
    const [isLoading, setIsLoading] = useState(false);

    const {setAuthTokens} = useAuth();

    const connect = (pseudo, password) => {
        if (pseudo === "" || password === "")
            setEmptyFieldAlert(true);
        else {
            if (rememberMe) {
                const hash = jwt.sign(
                    {password},
                    process.env.REACT_APP_TOKEN_SECRET
                );
                localStorage.setItem("pseudo", pseudoValue);
                localStorage.setItem("password", hash);
            } else {
                localStorage.removeItem("pseudo");
                localStorage.removeItem("password");
            }
            setIsLoading(true);
            api.loginUser(pseudo).then(result => {
                setIsLoading(false);
                if (result.status === 200) {
                    bcrypt.compare(password, retrievedFromJwt(result.data.token).findUser.password, function (err, res) {
                        if (res) {
                            setAuthTokens(result.data.token);
                            setLoggedIn(true);
                        }
                    });
                } else {
                    setWrongAccountAlert(true);
                }
            }).catch(e => {
                setIsLoading(false);
                setConnexionIssueAlert(true);
            });
        }
    }

    useEffect(() => {
        if (localStorage.getItem("pseudo"))
            setPasswordValue(retrievedFromJwt(localStorage.getItem("pseudo")));
        if (localStorage.getItem("password"))
            setPasswordValue(retrievedFromJwt(localStorage.getItem("password")).password);
    }, []);

    if (isLoggedIn) {
        if (localStorage.getItem("route")) {
            window.location.href = localStorage.getItem("route");
        } else {
            window.location.href = "/";
        }
    }

    const loadingFeedback = <>
        Chargement <PulseLoader color={"#1E1E1B"} size={4} margin={1}/>
    </>

    return (
        <>
            <Helmet>
                <title>APPETIT - Connexion</title>
            </Helmet>
            <div className="login-page-background">
                <div>
                    <h1 className="page-title">Bon APPETIT</h1>
                    <hr className="page-subline"/>
                    <h2 className="login-page-subtitle">Bienvenue</h2>
                </div>
                <div className="login-form">
                    <h4 className="form-title">CONNEXION</h4>
                    <p>La connexion est nécessaire pour pouvoir accéder à nos services.</p>
                    <InputGroup controlId={"input-login"}
                                label={"Identifiant"}
                                required={emptyFieldAlert}
                                handleKeyDown={(e) => e.keyCode === 13 ? document.getElementById("input-password").focus() : null}
                                value={pseudoValue}
                                handleOnChange={setPseudoValue}/>
                    <InputGroup controlId={"input-password"}
                                label={"Mot de passe"}
                                type={"password"}
                                required={emptyFieldAlert}
                                handleKeyDown={(e) => e.keyCode === 13 ? connect(pseudoValue, passwordValue) : null}
                                value={passwordValue}
                                handleOnChange={setPasswordValue}/>
                    {
                        wrongAccountAlert &&
                        <div className="login-form-alert">
                            <img className="alert-icon" src={alert} alt={"ALERTE"}/>
                            Ces identifiants sont incorrects.
                        </div>
                    }
                    {
                        connexionIssueAlert &&
                        <div className="login-form-alert">
                            <img className="alert-icon" src={alert} alt={"ALERTE"}/>
                            Une erreur est survenue, veuillez réessayer.
                        </div>
                    }
                    <Row sm={2} xs={1}>
                        <Col>
                            <InputCheckBox controlId={"input-remember-me"}
                                           label={"Se souvenir de moi"}
                                           value={rememberMe}
                                           handleOnChange={setRememberMe}/>
                        </Col>
                        <Col className="text-right">
                            <Button title={"Mot de passe oublié ?"} variant={"tertiary"} className={"py-0 px-2"}/>
                        </Col>
                    </Row>
                    <div className="text-center my-3">
                        <Button title={isLoading ? loadingFeedback : "CONNEXION"}
                                iconRight={isLoading ? null : arrowRight}
                                variant={"primary"}
                                handleOnClick={() => connect(pseudoValue, passwordValue)}/>
                    </div>
                </div>
            </div>
        </>
    );
}

