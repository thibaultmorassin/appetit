import React, {useState, useEffect, useContext} from "react";
import './ShoppingListPage.css';
import {Row, Col, Container} from "react-bootstrap";
import {Helmet} from "react-helmet";
import Footer from "../../components/Footer/Footer";
import NavbarAppetit from "../../components/NavbarAppetit/NavbarAppetit";
import PictureBand from "../../components/PictureBand/PictureBand";
import listBackground from "../../images/list-background.png";
import Button from "../../components/Button/Button";
import GlobalState from "../../context/GlobalState";
import {cryptData, decryptData} from "../../functions/users-infos";
import Popup from "../../components/Popup/Popup";
import check from "../../images/icons/check.svg";
import apiList from "../../api/api-list";
import SyncLoader from "react-spinners/SyncLoader";
import plusIcon from "../../images/icons/plus.svg";
import ListElement from "../../components/ListElement/ListElement";
import ShoppingListFilter from "../../components/ShoppingListFilter/ShoppingListFilter";
import InputGroup from "../../components/InputGroup/InputGroup";
import deleteIcon from "../../images/icons/x.svg";
import PulseLoader from "react-spinners/PulseLoader";
import {
    AiOutlineGroup,
    AiOutlineUngroup,
    AiOutlineSortAscending,
    AiOutlineSortDescending,
    AiOutlineUndo
} from "react-icons/ai";
import {IoArrowUndo} from "react-icons/io5";
import {TiArrowUnsorted} from "react-icons/ti";
import {FiTrash2} from "react-icons/fi";

export default function MenuOfTheWeek() {
    const globalState = useContext(GlobalState);
    const [showAddListItemPopup, setShowAddListItemPopup] = useState(false);
    const [showDeleteListPopup, setShowDeleteListPopup] = useState(false);

    const [shoppingList, setShoppingList] = useState({});
    const [shoppingListFilter, setShoppingListFilter] = useState("all");
    const [isListLoading, setIsListLoading] = useState(true);
    const [addingNewListItemLoader, setAddingNewListItemLoader] = useState(false);
    const [deletingListLoader, setDeletingListLoader] = useState(false);

    const [addListLabelValue, setAddListLabelValue] = useState("");
    const [addListQuantityValue, setAddListQuantityValue] = useState("");
    const [addListUnityValue, setAddListUnityValue] = useState("");

    const [isListGrouped, setIsListGrouped] = useState(false);
    const [isListSorted, setIsListSorted] = useState(null);
    const [showResetArrow, setShowResetArrow] = useState(false);
    const [showUndoArrow, setShowUndoArrow] = useState(false);

    const getList = () => {
        apiList.getList().then(result => {
            if (result.status === 200) {
                setShoppingList(decryptData(result.data.list).findList);
                setIsListLoading(false);
            }
        })
    }

    useEffect(() => {
        getList()
    }, []);

    const addListItem = () => {
        setAddingNewListItemLoader(true);
        let newItem = {
            quantity: addListQuantityValue,
            unity: addListUnityValue,
            label: addListLabelValue,
            type: "addedItems",
        }
        apiList.newList(cryptData(newItem)).then(result => {
            if (result.status === 200) {
                setAddingNewListItemLoader(false);
                setAddListLabelValue("");
                setAddListQuantityValue("");
                setAddListUnityValue("");
                getList();
                setShowAddListItemPopup(false);
            }
        });
    }

    const filterShoppingList = (type) => {
        setShoppingListFilter(type);
        setIsListLoading(true);
        apiList.filterList(type).then(result => {
            if (result.status === 200) {
                setShoppingList(decryptData(result.data.list).findList);
                setIsListLoading(false);
            }
        })
    }

    const handleDelete = (item) => {
        const itemId = item._id;
        localStorage.setItem("lastDeletedItem", JSON.stringify(item));
        if (typeof itemId === "string")
            return apiList.deleteItem(itemId).then(result => {
                if (result.status === 200) {
                    setShoppingList(decryptData(result.data.list).findList);
                    setIsListLoading(false);
                }
            });
        else itemId.map((item) => {
            return apiList.deleteItem(item).then(result => {
                if (result.status === 200) {
                    setShoppingList(decryptData(result.data.list).findList);
                    setIsListLoading(false);
                }
            });
        })
        setShowUndoArrow(true);
        setIsListGrouped(false);
    }

    const deleteList = () => {
        setDeletingListLoader(true);
        setIsListLoading(true);
        apiList.deleteList().then(result => {
            if (result.status === 200) {
                setShoppingList({});
                setIsListLoading(false);
                setShowDeleteListPopup(false);
                setDeletingListLoader(false);
            }
        })
    }

    const cancelItemDeletion = () => {
        const item = JSON.parse(localStorage.getItem("lastDeletedItem"));
        setAddingNewListItemLoader(true);
        apiList.newList(cryptData(item)).then(result => {
            if (result.status === 200) {
                setAddingNewListItemLoader(false);
                getList();
            }
        });
        setShowUndoArrow(false);
    }

    const resetListDisplay = () => {
        getList();
        setShowResetArrow(false);
        setIsListGrouped(false);
        setIsListSorted(null);
    }

    const displayGroupedList = () => {
        setShowResetArrow(true);
        setIsListGrouped(true);
        let groupedShoppingList = [];
        shoppingList.map((listElement) => {
            var itemAdded = false;
            groupedShoppingList.map((groupedElement) => {
                if (groupedElement.label.toUpperCase() === listElement.label.toUpperCase() && groupedElement.unity.toUpperCase() === listElement.unity.toUpperCase()) {
                    var newElement = groupedElement;
                    newElement._id = [newElement._id, listElement._id];
                    newElement.quantity = parseFloat(newElement.quantity) + parseFloat(listElement.quantity);
                    groupedShoppingList.push(newElement);
                    itemAdded = true;
                }
            })
            if (!itemAdded)
                groupedShoppingList.push(listElement);
        })
        groupedShoppingList = Array.from(new Set(groupedShoppingList));
        setShoppingList(groupedShoppingList);
    }

    const displayUngroupedList = () => {
        setIsListGrouped(false);
        getList();
        if (isListSorted === true)
            displayUnsortedList();
        else if (isListSorted === false)
            displaySortedList();
    }

    const displaySortedList = () => {
        setShowResetArrow(true);
        setShoppingList(shoppingList.sort((a, b) => a.label.localeCompare(b.label)));
        setIsListSorted(false);
    }

    const displayUnsortedList = () => {
        setShowResetArrow(true);
        setShoppingList(shoppingList.sort((a, b) => b.label.localeCompare(a.label)));
        setIsListSorted(true);
    }

    const addListItemPopupContent =
        <div className="px-3">
            <h5 className="text-center font-weight-bold">Ajouter un item à la liste</h5>
            <hr/>
            <InputGroup controlId={"input-add-list-item-label"}
                        label={"Nom de l'item"}
                        value={addListLabelValue}
                        transparent={true}
                        handleOnChange={setAddListLabelValue}/>
            <Row>
                <Col>
                    <InputGroup controlId={"input-add-list-item-quantity"}
                                label={"Quantité"}
                                value={addListQuantityValue}
                                transparent={true}
                                handleOnChange={setAddListQuantityValue}/>
                </Col>
                <Col>
                    <InputGroup controlId={"input-add-list-item-unity"}
                                label={"Unité"}
                                value={addListUnityValue}
                                transparent={true}
                                handleKeyDown={(e) => e.keyCode === 13 ? addListItem() : null}
                                handleOnChange={setAddListUnityValue}/>
                </Col>
            </Row>
            <Row className="text-center">
                <Col md={6} className={globalState.isMobile ? "my-3" : ""}>
                    <Button title={"Annuler"}
                            width={globalState.isMobile ? "100%" : ""}
                            iconRight={deleteIcon}
                            variant={"secondary"}
                            handleOnClick={() => setShowAddListItemPopup(false)}
                    />
                </Col>
                <Col md={6}>
                    <Button title={addingNewListItemLoader ? "Ajout..." : "Valider"}
                            width={globalState.isMobile ? "100%" : ""}
                            iconRight={addingNewListItemLoader
                                ? <PulseLoader color={"#1E1E1B"} size={4} margin={1}/>
                                : check}
                            variant={"black"}
                            handleOnClick={() => addListItem()}
                    />
                </Col>
            </Row>
        </div>;


    const deleteListPopupContent =
        <div className="px-3">
            <h5 className="text-center font-weight-bold">Vider la liste</h5>
            <hr/>
            <Container>
                <p>Vider la liste engendrera la perte de toutes les données. Êtes-vous sûr de vouloir continuer ?</p>
            </Container>
            <Row className="text-center">
                <Col md={6} className={globalState.isMobile ? "my-3" : ""}>
                    <Button title={"Annuler"}
                            width={globalState.isMobile ? "100%" : ""}
                            iconRight={deleteIcon}
                            variant={"secondary"}
                            handleOnClick={() => setShowDeleteListPopup(false)}
                    />
                </Col>
                <Col md={6}>
                    <Button title={deletingListLoader ? "Suppression..." : "Supprimer"}
                            width={globalState.isMobile ? "100%" : ""}
                            iconRight={deletingListLoader
                                ? <PulseLoader color={"#1E1E1B"} size={4} margin={1}/>
                                : check}
                            variant={"black"}
                            handleOnClick={deleteList}
                    />
                </Col>
            </Row>
        </div>;

    return (
        <>
            {
                globalState.isMobile &&
                <div className="btn-added-to-navbar">
                    <Button title={"Nouvel item"}
                            iconLeft={plusIcon}
                            width={"100%"}
                            variant={"primary"}
                            handleOnClick={() => setShowAddListItemPopup(true)}
                    />
                </div>
            }
            <Helmet>
                <title>APPETIT - Liste de courses</title>
            </Helmet>
            <div className="apt-page">
                {
                    showAddListItemPopup &&
                    <Popup onClose={() => setShowAddListItemPopup(false)}>
                        <div className="popup-add-list-item">
                            {addListItemPopupContent}
                        </div>
                    </Popup>
                }
                {
                    showDeleteListPopup &&
                    <Popup onClose={() => setShowDeleteListPopup(false)}>
                        <div className="popup-add-list-item">
                            {deleteListPopupContent}
                        </div>
                    </Popup>
                }
                <NavbarAppetit/>
                {
                    !globalState.isMobile &&
                    <PictureBand backgroundPicture={listBackground} height={'35vh'}>
                        <h1 className="page-title">LE BONHEUR EN LISTE</h1>
                        <hr className="page-subline"/>
                    </PictureBand>
                }
                <Container>
                    <div className={globalState.isMobile ? "py-3" : "apt-container my-3"}>
                        <h5 className={globalState.isMobile ? "mobile-shopping-page-title" : "apt-container-title mb-2 mt-1"}>
                            La liste de courses
                            {(shoppingList.length === 0) && <>{globalState.isMobile ? " - actuellement vide" : " actuellement vide"}</>}
                            {
                                (shoppingList.length !== 0) && !isListLoading &&
                                <> {globalState.isMobile ? <br/> : " - "}{shoppingList.length} items
                                    {(shoppingListFilter === "generated") && " pour les recettes"}
                                    {(shoppingListFilter === "addedItems") && " ajoutés"}
                                </>
                            }
                        </h5>
                        <Row>
                            <Col md={6}>
                                <ShoppingListFilter title={"Tout"}
                                                    active={shoppingListFilter === "all"}
                                                    handleOnClick={() => filterShoppingList("all")}
                                />
                                <ShoppingListFilter title={"Courses pour les recettes"}
                                                    active={shoppingListFilter === "generated"}
                                                    handleOnClick={() => filterShoppingList("generated")}
                                />
                                <ShoppingListFilter title={"Mes items ajoutés"}
                                                    active={shoppingListFilter === "addedItems"}
                                                    handleOnClick={() => filterShoppingList("addedItems")}
                                />
                                {
                                    !globalState.isMobile &&
                                    <div className="add-shopping-list-item-web">
                                        <Button title={"Nouvel item"}
                                                iconLeft={plusIcon}
                                                width={"100%"}
                                                variant={"secondary"}
                                                handleOnClick={() => setShowAddListItemPopup(true)}
                                        />
                                    </div>
                                }
                            </Col>
                            <Col md={6} className="position-relative">
                                <div
                                    className={"shopping-list-controls" + ((!shoppingList.length && !isListLoading) ? " invisible" : "")}>
                                    <FiTrash2 size={24}
                                              color={"#1e1e1b"}
                                              className={"cursor-pointer"}
                                              onClick={() => setShowDeleteListPopup(true)}/>
                                    <AiOutlineUndo size={24}
                                                   color={"#1e1e1b"}
                                                   className={showResetArrow ? "visible" : "invisible"}
                                                   onClick={resetListDisplay}/>
                                    {isListGrouped
                                        ? <AiOutlineUngroup size={24}
                                                            color={"#1e1e1b"}
                                                            className={"ml-auto mr-1 cursor-pointer"}
                                                            onClick={displayUngroupedList}/>
                                        : <AiOutlineGroup size={24}
                                                          color={"#1e1e1b"}
                                                          className={"ml-auto mr-1 cursor-pointer"}
                                                          onClick={displayGroupedList}/>}
                                    {isListSorted
                                        ? <AiOutlineSortAscending size={24}
                                                                  color={"#1e1e1b"}
                                                                  className={"mx-1 cursor-pointer"}
                                                                  onClick={displaySortedList}/>
                                        : (isListSorted === null
                                            ? <TiArrowUnsorted size={24}
                                                               color={"#1e1e1b"}
                                                               className={"mx-1 cursor-pointer"}
                                                               onClick={displaySortedList}/>
                                            : <AiOutlineSortDescending size={24}
                                                                       color={"#1e1e1b"}
                                                                       className={"mx-1 cursor-pointer"}
                                                                       onClick={displayUnsortedList}/>)}
                                    <IoArrowUndo size={24}
                                                 color={"#1e1e1b"}
                                                 className={showUndoArrow ? "visible mx-1 cursor-pointer" : "invisible mx-1 cursor-pointer"}
                                                 onClick={cancelItemDeletion}/>
                                </div>
                                <div className="shopping-list-container apt-scroll apt-scroll-hidden">
                                    <div className="shopping-list-container-bg"/>
                                    {
                                        isListLoading &&
                                        <div className="w-100 text-center mt-5 pt-5">
                                            <SyncLoader color={"#1E1E1B"} size={8}/>
                                        </div>
                                    }
                                    {
                                        !shoppingList.length && !isListLoading &&
                                        <div className="text-center">
                                            <Button title={"Ajouter un item"}
                                                    iconLeft={plusIcon}
                                                    variant={"secondary"}
                                                    handleOnClick={() => setShowAddListItemPopup(true)}
                                            />
                                        </div>
                                    }
                                    {
                                        !isListLoading && Object.keys(shoppingList).map((list, i) => {
                                            return <ListElement checked={true}
                                                                quantity={shoppingList[list].quantity}
                                                                unity={shoppingList[list].unity}
                                                                title={shoppingList[list].label}
                                                                handleDelete={() => handleDelete(shoppingList[list])}
                                                                key={i}
                                            />
                                        })
                                    }
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Container>
                <Footer/>
            </div>
        </>
    );
}

