import React, {useEffect, useState} from "react";
import './HomePage.css';
import NavbarAppetit from "../../components/NavbarAppetit/NavbarAppetit";
import Button from "../../components/Button/Button";
import arrowRight from "../../images/icons/arrow-right.svg";
import plusIcon from "../../images/icons/plus.svg";
import {Row, Col, Container} from "react-bootstrap";
import Footer from "../../components/Footer/Footer";
import PictureBand from "../../components/PictureBand/PictureBand";
import homePageBackground from "../../images/homepage-background.png";
import DetailledMenuCard from "../../components/DetailledMenuCard/DetailledMenuCard";
import ListElement from "../../components/ListElement/ListElement";
import apiMenu from "../../api/api-menu";
import apiList from "../../api/api-list";
import {cryptData, decryptData} from "../../functions/users-infos";
import {getFullDate} from "../../functions/date-function";
import SyncLoader from "react-spinners/SyncLoader";
import {Helmet} from "react-helmet";

export default function HomePage() {
    const [todayMenuValue, setTodayMenuValue] = useState({});
    const [shoppingListValue, setShoppingListValue] = useState({});
    const [isListLoading, setIsListLoading] = useState(true);
    const [isMenuLoading, setIsMenuLoading] = useState(true);

    useEffect(() => {
        apiMenu.getTodayMenu().then(result => {
            if (result.status === 200) {
                setTodayMenuValue(decryptData(result.data.todayMenu).findRecipe);
                setIsMenuLoading(false);
            }
        })
        apiList.getList().then(result => {
            if (result.status === 200) {
                setShoppingListValue(decryptData(result.data.list).findList);
                setIsListLoading(false);
            }
        })
    }, []);


    const handleDelete = (item) => {
        const itemId = item._id;
        localStorage.setItem("lastDeletedItem", JSON.stringify(item));
        if (typeof itemId === "string")
            apiList.deleteItem(itemId).then(result => {
                if (result.status === 200) {
                    setShoppingListValue(decryptData(result.data.list).findList);
                    setIsListLoading(false);
                }
            });
        else itemId.map((item) => {
            apiList.deleteItem(item).then(result => {
                if (result.status === 200) {
                    setShoppingListValue(decryptData(result.data.list).findList);
                    setIsListLoading(false);
                }
            });
        })
    }

    return (
        <div className="apt-page">
            <Helmet>
                <title>APPETIT - Accueil</title>
            </Helmet>
            <NavbarAppetit/>
            <PictureBand backgroundPicture={homePageBackground} height={'40vh'}>
                <h1 className="page-title">Bon APPETIT</h1>
                <hr className="page-subline"/>
                <div className="text-center mt-5">
                    <Button title={"VOIR LES RECETTES"}
                            iconRight={arrowRight}
                            variant={"translucid"}
                            handleOnClick={() => (window.location = "/recettes")}
                    />
                </div>
            </PictureBand>
            <Container>
                <div className="apt-container my-3">
                    <h5 className="apt-container-title mb-3">Le menu d'aujourd'hui - {getFullDate(new Date())}</h5>
                    <Row>
                        <Col md={6}>
                            <p className="ml-3 mb-1">MIDI</p>
                            <div className="homepage-recipes-container">
                                {
                                    isMenuLoading &&
                                    <div className="w-100 text-center my-3">
                                        <SyncLoader color={"#1E1E1B"} size={8}/>
                                    </div>
                                }
                                {
                                    Object.keys(todayMenuValue).map((recipe, i) => {
                                        return todayMenuValue !== {} && todayMenuValue[recipe].moment === "MIDI" &&
                                            <DetailledMenuCard recipe={todayMenuValue[recipe]} key={i}/>
                                    })
                                }
                            </div>
                        </Col>
                        <Col md={6}>
                            <p className="ml-3 mb-1">SOIR</p>
                            {
                                isMenuLoading &&
                                <div className="w-100 text-center my-3">
                                    <SyncLoader color={"#1E1E1B"} size={8}/>
                                </div>
                            }
                            {
                                Object.keys(todayMenuValue).map((recipe, i) => {
                                    return todayMenuValue !== {} && todayMenuValue[recipe].moment === "SOIR" &&
                                        <DetailledMenuCard recipe={todayMenuValue[recipe]} key={i}/>
                                })
                            }
                        </Col>
                    </Row>
                </div>
                <div className="apt-container mt-3 mb-5">
                    <h5 className="apt-container-title mb-3">La liste de courses</h5>
                    <Row>
                        <Col md={6}>
                            {
                                !shoppingListValue.length &&
                                <p>La liste de courses est actuellement vide.</p>
                            }
                            {
                                shoppingListValue.length &&
                                <p>Il y a actuellement <span
                                    className="font-weight-bold">{shoppingListValue.length}</span> items à acheter.</p>
                            }
                            <div className="text-center my-3">
                                <Button title={"VOIR LA LISTE"}
                                        iconRight={arrowRight}
                                        variant={"primary"}
                                        handleOnClick={() => (window.location = "/liste")}
                                />
                            </div>
                        </Col>
                        <Col md={6}>
                            <div className="homepage-list-container apt-scroll">
                                {
                                    isListLoading &&
                                    <div className="w-100 text-center mt-5 pt-5">
                                        <SyncLoader color={"#1E1E1B"} size={8}/>
                                    </div>
                                }
                                {
                                    !shoppingListValue.length && !isListLoading &&
                                    <div className="text-center">
                                        <Button title={"Ajouter un item"}
                                                iconLeft={plusIcon}
                                                variant={"secondary"}
                                                handleOnClick={() => (window.location = "/liste")}
                                        />
                                    </div>
                                }
                                {
                                    Object.keys(shoppingListValue).map((list, i) => {
                                        return <ListElement checked={true}
                                                            quantity={shoppingListValue[list].quantity}
                                                            unity={shoppingListValue[list].unity}
                                                            title={shoppingListValue[list].label}
                                                            handleDelete={() => handleDelete(shoppingListValue[list])}
                                                            key={i}
                                        />
                                    })
                                }
                            </div>
                        </Col>
                    </Row>
                </div>
            </Container>
            <Footer/>
        </div>
    );
}