import React, {useContext, useEffect, useState} from "react";
import '../AddRecipe/AddRecipe.css';
import {Row, Col, Container} from "react-bootstrap";
import Footer from "../../components/Footer/Footer";
import NavbarAppetit from "../../components/NavbarAppetit/NavbarAppetit";
import AddRecipeStepIndex from "../../components/AddRecipeStepIndex/AddRecipeStepIndex";
import ImagePicker from "../../components/ImagePicker/ImagePicker";
import InputGroup from "../../components/InputGroup/InputGroup";
import SelectGroup from "../../components/SelectGroup/SelectGroup";
import SelectGroupItem from "../../components/SelectGroupItem/SelectGroupItem";
import Button from "../../components/Button/Button";
import arrowRight from "../../images/icons/arrow-right.svg";
import GuestNumberPicker from "../../components/GuestNumberPicker/GuestNumberPicker";
import AddIngredientInputGroup from "../../components/AddIngredientInputGroup/AddIngredientInputGroup";
import GlobalState from "../../context/GlobalState";
import SwipeToDelete from "react-swipe-to-delete-component";
import AddRecipeStep from "../../components/AddRecipeStep/AddRecipeStep";
import RecipeStepsItem from "../../components/RecipeStepsItem/RecipeStepsItem";
import Popup from "../../components/Popup/Popup";
import PulseLoader from "react-spinners/PulseLoader";
import * as Icon from "react-feather";
import apiRecipe from "../../api/api-recipe";
import {cryptData, decryptData} from "../../functions/users-infos";
import categoryLabels from "../../functions/category-labels";
import tagLabels from "../../functions/tag-labels";
import TagFilter from "../../components/TagFilter/TagFilter";
import {Helmet} from "react-helmet";

export default function UpdateRecipe() {
    const globalState = useContext(GlobalState);
    const [isLoading, setIsLoading] = useState(false);
    const [success, setSuccess] = useState(false);
    const [fail, setFail] = useState(false);

    const [stepIndex, setStepIndex] = useState(1);
    const [emptyFieldAlert, setEmptyFieldAlert] = useState(false);
    const [showValidationPopup, setShowValidationPopup] = useState(false);

    const [recipeImage, setRecipeImage] = useState("");
    const [recipeTitle, setRecipeTitle] = useState("");
    const [recipeDescription, setRecipeDescription] = useState("");
    const [recipeDuration, setRecipeDuration] = useState(null);
    const [recipeCategory, setRecipeCategory] = useState("");
    const [recipePrice, setRecipePrice] = useState(null);
    const [recipeDifficulty, setRecipeDifficulty] = useState(null);

    const [fakeRecipeIngredients, setFakeRecipeIngredients] = useState([]);
    const [recipeIngredients, setRecipeIngredients] = useState([]);
    const [recipeTags, setRecipeTags] = useState([]);

    const [fakeRecipeStepContent, setFakeRecipeStepContent] = useState([]);
    const [recipeStepContent, setRecipeStepContent] = useState([]);

    const [guestNumber, setGuestNumber] = useState(4);

    const getRecipe = () => {
        apiRecipe.getRecipeById(localStorage.getItem("recipe")).then(result => {
            if (result.status === 200) {
                setRecipeImage(decryptData(result.data.recipe).findRecipe.picture)
                setRecipeTitle(decryptData(result.data.recipe).findRecipe.title);
                setRecipeDescription(decryptData(result.data.recipe).findRecipe.description);
                setRecipeDuration(decryptData(result.data.recipe).findRecipe.duration);
                setRecipeCategory(decryptData(result.data.recipe).findRecipe.category);
                setRecipePrice(decryptData(result.data.recipe).findRecipe.price);
                setRecipeDifficulty(decryptData(result.data.recipe).findRecipe.difficulty);
                setRecipeIngredients(decryptData(result.data.recipe).findRecipe.ingredients);
                setRecipeTags(decryptData(result.data.recipe).findRecipe.tag);
                setRecipeStepContent(decryptData(result.data.recipe).findRecipe.step);
                setGuestNumber(1);
            }
        })
    }

    useEffect(() => {
        if (localStorage.getItem("recipe") !== null)
            getRecipe();
    }, []);

    const handleFirstPhase = () => {
        if (recipeImage === "" || recipeTitle === "" || recipeDescription === "" || !recipeDuration || !recipeCategory || !recipePrice || !recipeDifficulty)
            setEmptyFieldAlert(true);
        else setStepIndex(2)
    };

    const handleSecondPhase = () => {
        if (recipeIngredients.length === 0)
            setEmptyFieldAlert(true);
        else setStepIndex(3)
    };

    const handleThirdPhase = () => {
        if (recipeStepContent.length === 0) {
            setEmptyFieldAlert(true);
        } else {
            setIsLoading(true);
            let recipe = {
                title: recipeTitle,
                category: recipeCategory,
                tag: recipeTags,
                description: recipeDescription,
                picture: recipeImage,
                difficulty: recipeDifficulty,
                duration: parseInt(recipeDuration),
                price: recipePrice,
                favoris: false,
                ingredients: recipeIngredients,
                step: recipeStepContent,
            }
            apiRecipe.updateRecipe(cryptData(recipe)).then(result => {
                setIsLoading(false);
                if (result.status === 200) {
                    localStorage.setItem("recipe", cryptData(decryptData(result.data.recipe).new_recipe));
                    setSuccess(true);
                } else {
                    setFail(true);
                }
            });
            setShowValidationPopup(true);
        }
    };

    const handleNewIngredientItem = (value) => {
        setFakeRecipeIngredients(value);
        setRecipeIngredients(value);
    };

    const handleDeleteIngredientItem = (ingredient) => {
        setRecipeIngredients(recipeIngredients.filter(item => item !== ingredient));
    };

    const handleNewRecipeStep = (value) => {
        setFakeRecipeStepContent(value);
        setRecipeStepContent(value);
    };

    const handleDeleteRecipeStep = (recipeStep) => {
        setRecipeStepContent(recipeStepContent.filter(item => item !== recipeStep));
    };

    const closePopup = () => {
        setIsLoading(true);
        if (success) {
            window.location = "/recette/" + recipeTitle;
        }
        setShowValidationPopup(false);
    }

    const recipeStepList = fakeRecipeStepContent.map((recipeStep, index) => {
            return <SwipeToDelete key={index} item={recipeStep}
                                  classNameTag={"animate__animated animate__fadeIn"}
                                  deleteSwipe={0.3}
                                  onDelete={() => handleDeleteRecipeStep(recipeStep)}>
                <div className="add-recipe-added-item">
                    <RecipeStepsItem step={recipeStep}
                                     stepIndex={index + 1}/>
                </div>
            </SwipeToDelete>
        }
    );

    const recipeIngredientList = fakeRecipeIngredients.map((ingredient, index) => {
            return <SwipeToDelete key={index} item={ingredient}
                                  classNameTag={"animate__animated animate__fadeIn animate__faster"}
                                  onDelete={() => handleDeleteIngredientItem(ingredient)}>
                <div className="add-recipe-added-item px-3">
                    <div className="flex-6">
                        {ingredient.label}
                    </div>
                    <div className="flex-3 pl-3 text-right">
                        {ingredient.quantity}
                    </div>
                    <div className="flex-2 pl-3 text-right">
                        {ingredient.unity}
                    </div>
                </div>
            </SwipeToDelete>
        }
    );

    const popupLoading =
        <>
            Chargement <PulseLoader color={"#1E1E1B"} size={4} margin={1}/>
        </>

    const popupSuccess =
        <>
            <h4 className="mb-3">Recette ajoutée !</h4>
            <img className="add-recipe-image" src={recipeImage} alt={recipeTitle}/>
            <h6>{recipeTitle}</h6>
            <p>La recette {recipeTitle} a bien été ajoutée à la base de données APP'ETIT.</p>
            <h4 className="mb-4">Merci !</h4>
        </>

    const popupFail =
        <>
            <h4 className="mb-3">Erreur !</h4>
            <p>Une erreur est survenue, veuillez réessayer.</p>
            <Icon.XOctagon color="#e71d36" size={48} className="animate__animated animate__pulse  animate__infinite"/>
        </>

    return (
        <div className={globalState.isMobile ? "apt-page bg-white" : "apt-page"}>
            <Helmet>
                <title>APPETIT - Modification de recette</title>
            </Helmet>
            <NavbarAppetit/>
            <Container>
                <div className={globalState.isMobile ? "py-3" : "apt-container my-3"}>
                    <AddRecipeStepIndex stepIndex={stepIndex} handleOnClick={setStepIndex}/>
                    {
                        stepIndex === 1 &&
                        <Row className="mt-3">
                            <Col md={5}>
                                <ImagePicker handleImage={setRecipeImage} required={emptyFieldAlert} src={recipeImage}/>
                            </Col>
                            <Col md={7}>
                                <InputGroup controlId={"input-title"}
                                            label={"Titre de la recette"}
                                            required={emptyFieldAlert}
                                            value={recipeTitle}
                                            handleOnChange={setRecipeTitle}/>
                                <InputGroup controlId={"input-description"}
                                            label={"Description de la recette"}
                                            required={emptyFieldAlert}
                                            value={recipeDescription}
                                            handleOnChange={setRecipeDescription}/>
                                <InputGroup controlId={"input-time"}
                                            label={"Durée totale (min) :"}
                                            type={"number"}
                                            required={emptyFieldAlert}
                                            value={recipeDuration}
                                            handleOnChange={setRecipeDuration}/>
                                <SelectGroup label={"Tags:"} required={emptyFieldAlert} value={recipeTags}>
                                    {
                                        Object.keys(tagLabels).map((index, i) => {
                                            return <TagFilter title={tagLabels[index]}
                                                              key={i}
                                                              handleOnClick={() => setRecipeTags([...recipeTags, tagLabels[index]])}
                                                              active={recipeTags.includes(tagLabels[index])}/>
                                        })}
                                </SelectGroup>
                                <SelectGroup label={"Catégorie :"} required={emptyFieldAlert} value={recipeCategory}>
                                    {
                                        Object.keys(categoryLabels).map((index, i) => {
                                            return <SelectGroupItem label={categoryLabels[index]}
                                                                    handleOnClick={() => setRecipeCategory(categoryLabels[index])}
                                                                    isSelected={recipeCategory === categoryLabels[index]}/>
                                        })}
                                </SelectGroup>
                                <SelectGroup label={"Coût :"} required={emptyFieldAlert} value={recipePrice}>
                                    <SelectGroupItem label={"€"} handleOnClick={() => setRecipePrice(1)}
                                                     isSelected={recipePrice === 1}/>
                                    <SelectGroupItem label={"€€"} handleOnClick={() => setRecipePrice(2)}
                                                     isSelected={recipePrice === 2}/>
                                    <SelectGroupItem label={"€€€"} handleOnClick={() => setRecipePrice(3)}
                                                     isSelected={recipePrice === 3}/>
                                </SelectGroup>
                                <SelectGroup label={"Difficulté :"} required={emptyFieldAlert} value={recipeDifficulty}>
                                    <SelectGroupItem label={"★"} handleOnClick={() => setRecipeDifficulty(1)}
                                                     isSelected={recipeDifficulty === 1}/>
                                    <SelectGroupItem label={"★★"} handleOnClick={() => setRecipeDifficulty(2)}
                                                     isSelected={recipeDifficulty === 2}/>
                                    <SelectGroupItem label={"★★★"} handleOnClick={() => setRecipeDifficulty(3)}
                                                     isSelected={recipeDifficulty === 3}/>
                                </SelectGroup>
                                <div className="w-50 ml-auto my-3 text-center">
                                    <Button title={"suivant"}
                                            iconRight={arrowRight}
                                            variant={"primary"}
                                            handleOnClick={() => handleFirstPhase()}
                                    />
                                </div>
                            </Col>
                        </Row>
                    }
                    {
                        stepIndex === 2 &&
                        <Row className="mt-3">
                            <Col md={4}>
                                <img className="add-recipe-image" src={recipeImage} alt={recipeTitle}/>
                            </Col>
                            <Col md={8}>
                                <div className="add-recipe-step-2-container">
                                    <h2 className="add-recipe-title">{recipeTitle}</h2>

                                    <div className="d-flex position-relative">
                                        <h6 className="ingredients-title">Ingrédients</h6>
                                        <div className="guest-number">
                                            <p className="guest-number-label">
                                                <span
                                                    className="font-weight-bold">{guestNumber}</span> personne{guestNumber === 1 ? "" : "s"}
                                            </p>
                                        </div>
                                        <div className="guest-number-picker-placeholder"/>
                                        <GuestNumberPicker guestNumber={guestNumber} setGuestNumber={setGuestNumber}/>
                                    </div>
                                    <div className="add-recipe-ingredients-container">
                                        <div className="add-recipe-ingredients-head">
                                            <div className="flex-6">
                                                Ingrédient
                                            </div>
                                            <div className="flex-3 text-right pl-3">
                                                Quantité
                                            </div>
                                            <div className="flex-2 text-right pl-3">
                                                Unité
                                            </div>
                                        </div>
                                        {recipeIngredientList}
                                        <div className="add-recipe-ingredients-body">
                                            <AddIngredientInputGroup value={recipeIngredients}
                                                                     required={emptyFieldAlert}
                                                                     guestNumber={guestNumber}
                                                                     handleNewElement={handleNewIngredientItem}/>
                                        </div>
                                    </div>
                                    <div className="w-50 ml-auto my-3 text-center">
                                        <Button title={"suivant"}
                                                iconRight={arrowRight}
                                                variant={"primary"}
                                                handleOnClick={() => handleSecondPhase()}
                                        />
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    }
                    {
                        stepIndex === 3 &&
                        <Row className="mt-3">
                            <Col md={4}>
                                <img className="add-recipe-image" src={recipeImage} alt={recipeTitle}/>
                            </Col>
                            <Col md={8}>
                                <div className="add-recipe-step-3-container">
                                    <h2 className="add-recipe-title">{recipeTitle}</h2>
                                    <h6 className="ingredients-title">Étapes</h6>
                                    <div className="pt-3 bg-light rounded overflow-hidden position-relative">
                                        {recipeStepList}
                                    </div>
                                    <AddRecipeStep value={recipeStepContent}
                                                   required={emptyFieldAlert}
                                                   handleNewElement={handleNewRecipeStep}/>
                                    <div className="add-recipe-validation">
                                        <Button title={"Valider la recette"}
                                                iconRight={arrowRight}
                                                variant={"primary"}
                                                handleOnClick={() => handleThirdPhase()}
                                        />
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    }
                    {
                        showValidationPopup &&
                        <Popup closeButton={isLoading} onClose={() => closePopup()}>
                            <div className="popup-add-recipe">
                                {isLoading && popupLoading}
                                {fail && popupFail}
                                {success && popupSuccess}
                            </div>
                        </Popup>
                    }
                </div>
            </Container>
            <Footer/>
        </div>
    );
}

