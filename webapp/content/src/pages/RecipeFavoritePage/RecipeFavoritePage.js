import React, {useContext, useEffect, useState} from "react";
import '../RecipeHomePage/RecipeHomePage.css';
import NavbarAppetit from "../../components/NavbarAppetit/NavbarAppetit";
import {Container, Row} from "react-bootstrap";
import Footer from "../../components/Footer/Footer";
import PictureBand from "../../components/PictureBand/PictureBand";
import homePageBackground from "../../images/homepage-background.png";
import SearchBar from "../../components/SearchBar/SearchBar";
import sliders from "../../images/icons/sliders.svg";
import CategoryFilter from "../../components/CategoryFilter/CategoryFilter";
import TagFilter from "../../components/TagFilter/TagFilter";
import Button from "../../components/Button/Button";
import plus from "../../images/icons/plus.svg";
import RecipeCard from "../../components/Recipe/RecipeCard/RecipeCard";
import {decryptData} from "../../functions/users-infos";
import apiRecipe from "../../api/api-recipe";
import GlobalState from "../../context/GlobalState";
import tagLabels from "../../functions/tag-labels";
import categoryLabels from "../../functions/category-labels";
import {Helmet} from "react-helmet";
import PulseLoader from "react-spinners/PulseLoader";

export default function RecipeFavoritePage() {
    const globalState = useContext(GlobalState);
    const [isLoading, setIsLoading] = useState(true);
    const [showFilterPopup, setShowFilterPopup] = useState(false);
    const [category, setCategory] = useState("");
    const [filters, setFilters] = useState([]);
    const [filteredRecipe, setFilteredRecipe] = useState({});

    useEffect(() => {
        apiRecipe.getFavorisRecipe("", "").then(result => {
            if (result.status === 200) {
                setFilteredRecipe(decryptData(result.data.recipe).findRecipe);
                setIsLoading(false);
            }
        })
    }, []);

    const filterRecipe = (category, filters) => {
        apiRecipe.getFavorisRecipe(category, filters).then(result => {
            if (result.status === 200) {
                setFilteredRecipe(decryptData(result.data.recipe).findRecipe);
            }
        })
    }

    const changeFilter = (label) => {
        if (filters.includes(label)) {
            setFilters(filters.filter(item => item !== label));
            filterRecipe(category, filters.filter(item => item !== label));
        } else {
            setFilters([...filters, label]);
            filterRecipe(category, [...filters, label]);
        }
    }

    const changeCategory = (label) => {
        if (category === label) {
            setCategory("");
            filterRecipe("", filters);
        } else {
            setCategory(label);
            filterRecipe(label, filters);
        }
    }

    const recipeFilters =
        <>
            <div className="d-flex">
                <SearchBar/>
                <img className="filter" src={sliders} draggable={false} alt={"FILTRER"}
                     onClick={() => setShowFilterPopup(true)}/>
            </div>
            <div className="filters-container apt-scroll">
                {
                    Object.keys(categoryLabels).map((index, i) => {
                        return <CategoryFilter title={categoryLabels[index]}
                                               key={i}
                                               handleOnClick={() => changeCategory(categoryLabels[index])}
                                               active={category === categoryLabels[index]}/>
                    })}
                {
                    Object.keys(tagLabels).map((index, i) => {
                        return <TagFilter title={tagLabels[index]}
                                          key={i}
                                          handleOnClick={() => changeFilter(tagLabels[index])}
                                          active={filters.includes(tagLabels[index])}/>
                    })}
            </div>
        </>

    const recipeFiltersMobile =
        <>
            <div className="d-flex">
                <SearchBar/>
                <img className="filter" src={sliders} draggable={false} alt={"FILTRER"}
                     onClick={() => setShowFilterPopup(true)}/>
            </div>
            <div className="filters-container apt-scroll apt-scroll-hidden p-0 my-2">
                {
                    Object.keys(categoryLabels).map((index, i) => {
                            return <CategoryFilter title={categoryLabels[index]}
                                                   key={i}
                                                   handleOnClick={() => changeCategory(categoryLabels[index])}
                                                   active={category === categoryLabels[index]}/>
                        }
                    )}
            </div>
            <div className="filters-container apt-scroll apt-scroll-hidden p-0 mt-0">
                {
                    Object.keys(tagLabels).map((index, i) => {
                        return <TagFilter title={tagLabels[index]}
                                          key={i}
                                          handleOnClick={() => changeFilter(tagLabels[index])}
                                          active={filters.includes(tagLabels[index])}/>
                    })}
            </div>
        </>

    const loader =
        <div className="text-center w-100 mt-5">
            <PulseLoader color={"#1E1E1B"} size={20}/>
        </div>

    return (
        <>
            <Helmet>
                <title>APPETIT - Recettes préférées</title>
            </Helmet>
            <div className="apt-page">
                <NavbarAppetit/>
                {
                    !globalState.isMobile &&
                    <PictureBand backgroundPicture={homePageBackground} height={'40vh'}>
                        <h1 className="page-title">RECETTES FAVORITES</h1>
                        <hr className="page-subline"/>
                        <div className="text-center mt-5">
                            <Button title={"AJOUTER DES FAVORIS"}
                                    iconLeft={plus}
                                    variant={"translucid"}
                                    handleOnClick={() => (window.location = "/recettes")}/>
                        </div>
                    </PictureBand>
                }
                <Container>
                    {
                        globalState.isMobile &&
                        <div className="pt-4">
                            {recipeFiltersMobile}
                            <Row className="recipes-container apt-scroll">
                                {
                                    isLoading && <div className="w-100 pt-5 mt-5">
                                        {loader}
                                    </div>
                                }
                                {
                                    Object.keys(filteredRecipe).map((recipe, i) => {
                                        return filteredRecipe !== {} &&
                                            <RecipeCard recipe={filteredRecipe[recipe]} key={i}/>
                                    })
                                }
                            </Row>
                        </div>
                    }
                    {
                        !globalState.isMobile &&
                        <div className="apt-container my-3">
                            {recipeFilters}
                            <Row className="recipes-container apt-scroll">
                                {
                                    isLoading && loader
                                }
                                {
                                    !isLoading &&
                                    Object.keys(filteredRecipe).map((recipe, i) => {
                                            return filteredRecipe !== {} &&
                                                <RecipeCard recipe={filteredRecipe[recipe]} key={i}/>
                                        }
                                    )
                                }
                            </Row>
                        </div>
                    }
                </Container>
            </div>
            <Footer/>
        </>
    );
}