const jwt = require("jsonwebtoken");

export function retrievedFromJwt(auth) {
    let res = null;
    if (auth) {
        jwt.verify(auth, process.env.REACT_APP_TOKEN_SECRET, function (err, decoded) {
            if (err) {
                res = null;
            } else {
                res = decoded;
            }
        });
    }
    return res;
}

export function decryptData(data) {
    let res = null;
    if (data) {
        jwt.verify(data, process.env.REACT_APP_TOKEN_SECRET, function (err, decoded) {
            if (err) {
                res = null;
            } else {
                res = decoded;
            }
        });
    }
    return res;
}

export function cryptData(data) {
    return jwt.sign(
        {data},
        process.env.REACT_APP_TOKEN_SECRET
    );
}

