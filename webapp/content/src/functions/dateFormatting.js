export const dateFormatting = (date) => {
    var weekday = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];
    var dayTitle = weekday[date.getDay()];
    var dateToString = ('0' + date.getDate()).slice(-2) + '/'
        + ('0' + (date.getMonth() + 1)).slice(-2) + '/'
        + date.getFullYear();
    return dayTitle + " " + dateToString;
}