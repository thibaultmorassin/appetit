const tagLabels = [
    "Viande",
    "Poisson",
    "Végétarien",
    "Pâtes",
    "Salade",
    "Soupe",
    "Asiatique",
]

export default tagLabels;