export const getFullDate = (date) => {
    var weekday = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];
    var dayTitle = weekday[date.getDay()];
    var dateToString = ('0' + date.getDate()).slice(-2) + '/'
        + ('0' + (date.getMonth() + 1)).slice(-2) + '/'
        + date.getFullYear();
    return dayTitle + " " + dateToString;
}

export const getDateLabel = (index) => {
    var weekday = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];
    return weekday[index];
}

export const getDateDDMMYY = (date) => {
    return ('0' + date.getDate()).slice(-2) + '/'
        + ('0' + (date.getMonth() + 1)).slice(-2) + '/'
        + date.getFullYear();
}

export const getThisWeeksDates = (today) => {
    const currentWeek = [];
    const current = new Date();
    current.setDate((current.getDate() - current.getDay() + 1 - today));
    for (var j = 0; j < 8; j++) {
        currentWeek.push(getDateDDMMYY(new Date(current)));
        current.setDate(current.getDate() + 1);
    }
    currentWeek[0] = currentWeek[7];
    return currentWeek;
}