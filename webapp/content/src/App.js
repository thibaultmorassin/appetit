import LoginPage from "./pages/LoginPage/LoginPage";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {useEffect, useState} from "react";
import {AuthContext} from "./context/authentificationContext";
import GlobalState from "./context/GlobalState";
import HomePage from "./pages/HomePage/HomePage";
import PrivateRoute from "./components/PrivateRoute/PrivateRoute";
import RecipePage from "./pages/RecipePage/RecipePage";
import AddRecipe from "./pages/AddRecipe/AddRecipe";
import RecipeFavoritePage from "./pages/RecipeFavoritePage/RecipeFavoritePage";
import UpdateRecipe from "./pages/UpdateRecipe/UpdateRecipe";
import MenuOfTheWeek from "./pages/MenuOfTheWeek/MenuOfTheWeek";
import RecipeHomePage from "./pages/RecipeHomePage/RecipeHomePage";
import ShoppingListPage from "./pages/ShoppingListPage/ShoppingListPage";


function App() {
    const existingTokens = JSON.parse(localStorage.getItem("tokens"));
    const [authTokens, setAuthTokens] = useState(existingTokens);
    const [isMobile, setIsMobile] = useState(window.innerWidth <= 992);

    const setTokens = (token) => {
        localStorage.setItem("tokens", JSON.stringify(token));
        setAuthTokens(token);
    }

    const initialState = {
        isMobile, setIsMobile
    };

    useEffect(() => {
        window.addEventListener('resize', () => setIsMobile(window.innerWidth <= 992));
        return () => {
            window.removeEventListener('resize', () => setIsMobile(window.innerWidth <= 992));
        }
    }, []);

    return (
        <AuthContext.Provider value={{authTokens, setAuthTokens: setTokens}}>
            <GlobalState.Provider value={initialState}>
                <Router>
                    <Switch>
                        <PrivateRoute exact path="/" component={HomePage}/>
                        <PrivateRoute exact path="/recettes" component={RecipeHomePage}/>
                        <PrivateRoute exact path="/recettes-favorites" component={RecipeFavoritePage}/>
                        <PrivateRoute exact path="/recette/:recipeName" component={RecipePage}/>
                        <PrivateRoute exact path="/recettes" component={RecipeHomePage}/>
                        <PrivateRoute exact path="/recette/:recipeId" component={RecipePage}/>
                        <PrivateRoute exact path="/ajouter-une-recette" component={AddRecipe}/>
                        <PrivateRoute exact path="/modifier-une-recette/:recipeName" component={UpdateRecipe}/>
                        <PrivateRoute exact path="/menu" component={MenuOfTheWeek}/>
                        <PrivateRoute exact path="/liste" component={ShoppingListPage}/>
                        <Route exact path="/connexion" component={LoginPage}/>
                        {/* 404 MUST be the last specified route, or the following routes will be redirected to 404 */}
                        {/*<Route component={Page404}/>*/}
                    </Switch>
                </Router>
            </GlobalState.Provider>
        </AuthContext.Provider>
    );
}

export default App;
