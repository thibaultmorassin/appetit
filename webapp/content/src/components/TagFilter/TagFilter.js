import React from "react";
import './TagFilter.css';

export default function TagFilter(props) {

    return (
        <div className={"tag-filter" + (props.active ? " tag-filter-active" : "") + (props.disabled ? " tag-filter-disabled" : "")}
             onClick={props.handleOnClick}>
            {props.title}
        </div>
    );
}   