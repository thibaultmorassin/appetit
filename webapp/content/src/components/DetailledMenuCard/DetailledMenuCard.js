import React, {useContext} from "react";
import './DetailledMenuCard.css';
import './DetailledMenuCardMobile.css';
import {Row} from "react-bootstrap";
import recipe from "../../images/recipe.png";
import RecipeIconInfos from "../Recipe/RecipeIconInfos/RecipeIconInfos";
import Button from "../Button/Button";
import arrowRight from "../../images/icons/arrow-right.svg";
import GlobalState from "../../context/GlobalState";

export default function DetailledMenuCard(props) {
    const globalState = useContext(GlobalState);

    return (
        <>
            {
                globalState.isMobile &&
                <div className="detailled-menu-card-mobile-container">
                    <div className="detailled-menu-card-mobile-span"/>
                    <div className="detailled-menu-card-mobile">
                        <div className="position-relative">
                            <img className="detailled-menu-card-mobile-picture" draggable={false}
                                 src={props.recipe.picture ? props.recipe.picture : recipe} alt={props.recipe.title}/>
                            <div className="detailled-menu-card-mobile-picture-gradient"/>
                        </div>
                        <div className="px-3 py-2">
                            <p className="detailled-menu-card-mobile-title">{props.recipe.title}</p>
                            {props.recipe.description && <>
                                <p className="detailled-menu-card-mobile-description">{props.recipe.description}</p>
                                <RecipeIconInfos recipe={props.recipe}/></>
                            }
                        </div>
                    </div>
                </div>

            }
            {
                !globalState.isMobile &&
                <div className="detailled-menu-card-container">
                    <div className="detailled-menu-card-span"/>
                    <div className="detailled-menu-card">
                        <Row className="pl-3">
                            <div className="p-2">
                                <img className="detailled-menu-card-picture" draggable={false}
                                     src={props.recipe.picture ? props.recipe.picture : recipe} alt={props.recipe.title}/>
                            </div>
                            <div className="detailled-menu-card-content">
                                <p className="font-weight-bold mb-2">{props.recipe.title}</p>
                                {props.recipe.description && <>
                                    <p className="detailled-menu-card-description">{props.recipe.description}</p>
                                    <RecipeIconInfos recipe={props.recipe}/>
                                    <div className="mt-2 text-center">
                                        <Button title={"VOIR LA RECETTE"}
                                                iconRight={arrowRight}
                                                variant={"primary"}/>
                                    </div>
                                </>
                                }
                            </div>
                        </Row>
                    </div>
                </div>
            }
        </>
    );
}

