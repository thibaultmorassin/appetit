import React, {useState} from "react";
import './InputGroup.css';
import eye from "../../images/icons/eye.svg"
import eyeOff from "../../images/icons/eye-off.svg"

export default function InputGroup(props) {
    const [passwordVisible, setPasswordVisible] = useState(false);
    const input = React.createRef();
    const handlePasswordVisible = () => {
        setPasswordVisible(!passwordVisible);
        input.current.type = (passwordVisible ? "text" : "password");
    }


    return (
        <div className="apt-input-group">
            <label className={"apt-input-label" + (props.transparent ? " apt-input-label-transparent" : "")}
                   htmlFor={props.controlId}>
                {props.label}
            </label>
            <input
                className={"form-control apt-input" + (props.value ? " not-empty" : " ") + (props.transparent && " bg-transparent border-dark")}
                type={props.type ? props.type : "text"}
                ref={input}
                id={props.controlId}
                placeholder={props.placeholder}
                onKeyDown={props.handleKeyDown}
                value={props.value} onChange={(event) => props.handleOnChange(event.target.value)}/>
            {
                props.type === "password" &&
                <img className="input-password" src={passwordVisible ? eye : eyeOff}
                     onClick={() => handlePasswordVisible()}
                     alt="Show password"/>
            }
            {
                props.required && !props.value &&
                <p className="apt-input-alert">Veuillez remplir ce champ.</p>
            }
        </div>

    );
}

