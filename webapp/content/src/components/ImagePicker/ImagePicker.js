import React, {useState, useCallback} from 'react'
import './ImagePicker.css';
import Slider from "@material-ui/core/Slider";
import Form from 'react-bootstrap/Form';
import Cropper from 'react-easy-crop';
import getCroppedImg from "./ImageCropper";
import deleteIcon from "../../images/icons/x-circle.svg";
import Button from "../Button/Button";
import check from "../../images/icons/check.svg";

export default function ImagePicker(props) {
    const [image, setImage] = useState(props.src ? props.src : null)
    const [crop, setCrop] = useState({x: 0, y: 0})
    const [zoom, setZoom] = useState(1)
    const [croppedAreaPixels, setCroppedAreaPixels] = useState(null)

    const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
        setCroppedAreaPixels(croppedAreaPixels)
    }, [])

    const showCroppedImage = useCallback(async () => {
        try {
            const croppedImage = await getCroppedImg(
                image,
                croppedAreaPixels
            )
            props.handleImage(croppedImage);
        } catch (e) {
            console.error(e)
        }
    }, [croppedAreaPixels])

    const onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                setImage(e.target.result);
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    }

    const onZoomChange = (zoom) => {
        setZoom(zoom);
    }

    return (
        <>
            {
                !image &&
                <div className="image-picker-input">
                    <Form className="w-100">
                        <Form.File
                            id="custom-file"
                            label="Choisir une image"
                            data-browse="Parcourir"
                            accept="image/png, image/jpeg"
                            onChange={onImageChange}
                            custom
                        />
                        {
                            props.required && !props.value &&
                            <p className="apt-select-group-alert">Veuillez sélectionner une image.</p>
                        }
                    </Form>
                </div>
            }
            {
                image &&
                <>
                    <div className="crop-container" onClick={showCroppedImage}>
                        <Cropper
                            image={image}
                            crop={crop}
                            zoom={zoom}
                            aspect={1}
                            onCropChange={setCrop}
                            onCropComplete={onCropComplete}
                            onZoomChange={setZoom}
                        />
                    </div>
                    <div className="crop-control">
                        <Slider
                            value={zoom}
                            min={1}
                            max={3}
                            step={0.1}
                            aria-labelledby="Zoom"
                            onChange={(e, zoom) => onZoomChange(zoom)}
                        />
                        <img alt="supprimer" className="delete-crop-image" onClick={() => setImage(null)}
                             src={deleteIcon}/>
                    </div>
                    <div className="my-3 text-center">
                        <Button title={"VALIDER CETTE IMAGE"}
                                iconRight={check}
                                variant={"secondary"}
                                handleOnClick={() => showCroppedImage()}
                        />
                    </div>
                    {
                        props.required && !props.value &&
                        <p className="apt-select-group-alert text-center">Veuillez valider cette image.</p>
                    }
                </>
            }
        </>
    );
}

