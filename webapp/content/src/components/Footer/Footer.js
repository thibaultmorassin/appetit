import React, {useContext} from "react";
import './Footer.css';
import logo from "../../images/Logo_white.svg";
import GlobalState from "../../context/GlobalState";

export default function Footer() {
    const globalState = useContext(GlobalState);

    return (
        <>
            {
                !globalState.isMobile &&
                <div className="apt-footer">
                    <img className="apt-footer-logo" draggable={false} src={logo} alt={"APPETIT"}/>
                </div>
            }
        </>
    );
}

