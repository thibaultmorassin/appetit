import React from "react";
import "./Notification.css"
import {Toast} from "react-bootstrap";
import trueCheckBox from "../../images/icons/checkbox-true.svg"

export default function Notification(props) {

    return (
        <Toast className="apt-notification-toast"
               onClose={props.handleOnClose}
               show={props.show}
               delay={5000}
               autohide>
            <Toast.Header>
                <img className="apt-notification-check" src={trueCheckBox} alt={"checkBox"}/>
                <strong>{props.title}</strong>
                <small className="ml-auto">{props.time}</small>
            </Toast.Header>
            <Toast.Body>
                {props.content}
            </Toast.Body>
        </Toast>
    );
}
