import React from "react";
import './SearchBar.css';

import search from '../../images/icons/search.svg';

export default function SearchBar() {

    return (
        <div className="search-bar">
            <img className="search-bar-icon" draggable={false} src={search} alt={"🔍"}/>
            <input className="search-bar-input" placeholder="Rechercher..."/>
        </div>
    );
}   