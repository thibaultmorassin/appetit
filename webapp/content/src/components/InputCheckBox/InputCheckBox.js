import React from "react";
import './InputCheckBox.css';
import trueCheckBox from "../../images/icons/checkbox-true.svg"
import falseCheckbox from "../../images/icons/checkbox-false.svg"

export default function InputCheckBox(props) {
    return (
        <div className="apt-check-container">
            <input value={props.value} type="checkbox"
                   onChange={() => props.handleOnChange(!props.value)}
                   id={props.controlId}/>
            <label className="apt-check-label" htmlFor={props.controlId}>{props.label}</label>
            <img className="apt-checkmark" src={props.value ? trueCheckBox : falseCheckbox}
                 onClick={() => props.handleOnChange(!props.value)}
                 alt={"checkBox"}/>
        </div>
    );
}

