import React from "react";
import Ripples from 'react-ripples';
import './Button.css';

export default function Button(props) {
    const ripplesStyles = {
        display: 'inline-flex',
        borderRadius: 19,
        overflow: 'hidden',
        textAlign: "center",
        width: (props.width ? "100%" : "auto"),
    };

    return (
        <div style={ripplesStyles}>
            <Ripples color={props.variant === "black" ? "rgba(250,250,250, .1)" : "rgba(0, 0, 0, .1)"}
                     style={{width: "100%"}} onClick={props.handleOnClick}>
                <div
                    className={"btn-apt btn-apt-" + props.variant + " " + (props.className ? props.className : "")
                    + (props.iconLeft ? " btn-with-icon-left" : "")}
                    style={{width: props.width}}>
                    {
                        props.iconLeft &&
                        <img className="btn-apt-icon-left" src={props.iconLeft} alt={""}/>
                    }
                    {props.title}
                    {
                        props.iconRight &&
                        <img className="btn-apt-icon-right" src={props.iconRight} alt={""}/>
                    }
                </div>
            </Ripples>
        </div>
    );
}

