import React from "react";
import './RecipeStepsItem.css';

export default function RecipeStepsItem(props) {
    return (
        <div className="recipe-step-item" onClick={props.handleOnClick}>
            <div className="recipe-step-item-index-container">
                {
                    !props.isLastStep &&
                    <div className={"recipe-step-item-index-line" + (props.isActive ? "-active" : "")}/>
                }
                <div className={"recipe-step-item-index" + (props.isActive ? "-active" : "")}>
                    <div className="m-auto">
                        {props.isLastStep ? "FIN" : props.stepIndex}
                    </div>
                </div>
            </div>
            <div className="recipe-step-item-label">
                {props.step.content}
            </div>
        </div>
    )

}   