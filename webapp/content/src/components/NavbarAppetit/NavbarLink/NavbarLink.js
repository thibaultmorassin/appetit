import React from "react";
import './NavbarLink.css';

export default function NavbarLink(props) {

    const redirect = (location) => {
        window.location = location;
    };

    return (
        <>
            <div onClick={() => redirect(props.href)}
                 className={"navbar-appetit-link" + (props.isActive ? " apt-active" : "") + (props.buttonLike ? " navbar-link-btn" : "")}>
                {props.title}
                {props.isActive &&
                <hr className="apt-active-subline"/>}
            </div>
        </>
    );
}

