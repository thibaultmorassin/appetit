import React, {useContext, useState} from "react";
import './NavbarAppetit.css';
import {Navbar, Container, Nav} from "react-bootstrap";
import NavbarLink from "./NavbarLink/NavbarLink";
import MobileNavbarLink from "./MobileNavbarLink/MobileNavbarLink";
import GlobalState from "../../context/GlobalState";
import {useRouteMatch} from "react-router-dom";
import logo from "../../images/Logo_white.svg";

export default function NavbarAppetit() {
    let path = useRouteMatch();
    const [activeLink] = useState(path.path);
    const globalState = useContext(GlobalState);
    const isRecipeActive = () => {
        return (activeLink === "/recettes"
            || activeLink === "/recette/:recipeName"
            || activeLink === "/ajouter-une-recette"
            || activeLink === "/modifier-une-recette/:recipeName");
    }

    return (
        <>
            {
                globalState.isMobile &&
                <div className="mobile-navbar-background">
                    <div className="mobile-navbar-appetit">
                        <div className="mobile-navbar-appetit-container">
                            <MobileNavbarLink href="/" isActive={activeLink === "/"} title={"Accueil"}/>
                            <MobileNavbarLink href="/recettes"
                                              isActive={isRecipeActive()}
                                              title={"Recettes"}/>
                            <MobileNavbarLink href="/menu" isActive={activeLink === "/menu"} title={"Menu"}/>
                            <MobileNavbarLink href="/liste" isActive={activeLink === "/liste"} title={"Liste"}/>
                            <MobileNavbarLink href="/recettes-favorites" isActive={activeLink === "/favoris"}
                                              title={"Favoris"}/>
                        </div>
                    </div>
                </div>
            }
            {
                !globalState.isMobile &&
                <Navbar expand="lg" className={"navbar-appetit"} fixed="top">
                    <Container>
                        <Navbar.Brand href="/">
                            <img
                                src={logo}
                                width="30"
                                height="30"
                                className="d-inline-block align-top"
                                alt="React Bootstrap logo"
                            /></Navbar.Brand>
                        <Nav className="w-100">
                            <NavbarLink href="/" isActive={activeLink === "/"} title={"Accueil"}/>
                            <NavbarLink href="/recettes" isActive={isRecipeActive()} title={"Recettes"}/>
                            <NavbarLink href="/menu" isActive={activeLink === "/menu"} title={"Menu de la semaine"}/>
                            <NavbarLink href="/liste" isActive={activeLink === "/liste"} title={"Liste de courses"}/>
                            <NavbarLink href="/recettes-favorites" isActive={activeLink === "/favoris"}
                                        buttonLike={true}
                                        title={"Recettes préférées"}/>
                        </Nav>
                    </Container>
                </Navbar>
            }
        </>
    );
}

