import React from "react";
import './MobileNavbarLink.css';

import homeActive from "../../../images/icons/home-active.svg";
import homeInactive from "../../../images/icons/home-inactive.svg";
import recipeActive from "../../../images/icons/recipe-active.svg";
import recipeInactive from "../../../images/icons/recipe-inactive.svg";
import menuActive from "../../../images/icons/menu-active.svg";
import menuInactive from "../../../images/icons/menu-inactive.svg";
import listActive from "../../../images/icons/list-active.svg";
import listInactive from "../../../images/icons/list-inactive.svg";
import favorisActive from "../../../images/icons/favoris-active.svg";
import favorisInactive from "../../../images/icons/favoris-inactive.svg";

export default function MobileNavbarLink(props) {
    const redirect = (location) => {
        window.location = location;
    };

    return (
        <>
            <div onClick={() => redirect(props.href)}
                 className={"mobile-navbar-appetit-link" + (props.isActive ? " apt-active" : "")}>
                {
                    props.title === "Accueil" && props.isActive &&
                    <img src={homeActive} alt={props.title}/>
                }
                {
                    props.title === "Accueil" && !props.isActive &&
                    <img src={homeInactive} alt={props.title}/>
                }
                {
                    props.title === "Recettes" && props.isActive &&
                    <img src={recipeActive} alt={props.title}/>

                }
                {
                    props.title === "Recettes" && !props.isActive &&
                    <img src={recipeInactive} alt={props.title}/>
                }
                {
                    props.title === "Menu" && props.isActive &&
                    <img src={menuActive} alt={props.title}/>
                }
                {
                    props.title === "Menu" && !props.isActive &&
                    <img src={menuInactive} alt={props.title}/>
                }
                {
                    props.title === "Liste" && props.isActive &&
                    <img src={listActive} alt={props.title}/>
                }
                {
                    props.title === "Liste" && !props.isActive &&
                    <img src={listInactive} alt={props.title}/>
                }
                {
                    props.title === "Favoris" && props.isActive &&
                    <img src={favorisActive} alt={props.title}/>
                }
                {
                    props.title === "Favoris" && !props.isActive &&
                    <img src={favorisInactive} alt={props.title}/>
                }
            </div>
        </>
    );
}

