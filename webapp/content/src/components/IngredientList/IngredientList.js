import React, {useContext, useState} from "react";
import './IngredientList.css';
import chevron from "../../images/icons/chevron-down.svg";
import Accordion from "react-bootstrap/cjs/Accordion";
import IngredientListItem from "../IngredientListItem/IngredientListItem";
import GlobalState from "../../context/GlobalState";
import GuestNumberPicker from "../GuestNumberPicker/GuestNumberPicker";

export default function IngredientList(props) {
    const [dropdownVisible, setDropdownVisible] = useState(true);
    const [guestNumber, setGuestNumber] = useState(2);
    const globalState = useContext(GlobalState);

    const ingredientsHeaderWeb =
        <div className="ingredients-header">
            <h6 className="ingredients-title">Ingrédients</h6>
            <div className="guest-number">
                <p className="guest-number-label">
                                <span
                                    className="font-weight-bold">{guestNumber}</span> personne{guestNumber === 1 ? "" : "s"}
                </p>
            </div>
            <GuestNumberPicker guestNumber={guestNumber} setGuestNumber={setGuestNumber}/>
            <Accordion.Toggle as={"div"} eventKey="0">
                <img
                    className={"ingredients-dropdown-buttons" + (dropdownVisible ? " dropdown-active" : "")}
                    onClick={() => setDropdownVisible(!dropdownVisible)}
                    src={chevron} draggable={false} alt={"v"}/>
            </Accordion.Toggle>
        </div>
    ;
    const ingredientsHeaderMobile =
        <div className="position-relative">
            <Accordion.Toggle as={"div"} eventKey="0">
                <div className="ingredients-header">
                    <h6 className="ingredients-title">Ingrédients</h6>
                    <div className="guest-number">
                        <p className="guest-number-label">
                                <span
                                    className="font-weight-bold">{guestNumber}</span> personne{guestNumber === 1 ? "" : "s"}
                        </p>
                    </div>
                    <div className="guest-number-picker-placeholder"/>
                </div>
            </Accordion.Toggle>
            <div className="guest-number-picker">
                <div className="guest-number-picker-button guest-less"
                     onClick={() => {
                         if (guestNumber !== 1)
                             setGuestNumber(guestNumber - 1)
                     }}>
                    -
                </div>
                <div className="guest-number-picker-button guest-plus"
                     onClick={() => setGuestNumber(guestNumber + 1)}>
                    +
                </div>
            </div>
        </div>
    ;

    return (
        <div className="apt-container-light pb-3">
            <Accordion defaultActiveKey="0">
                {
                    globalState.isMobile &&
                    ingredientsHeaderMobile
                }
                {
                    !globalState.isMobile &&
                    ingredientsHeaderWeb
                }
                <Accordion.Collapse eventKey="0">
                    <div>
                        {
                            props.ingredients !== undefined &&
                            props.ingredients.map((ingredient, index) => {
                                return <IngredientListItem ingredient={ingredient}
                                                           guestNumber={guestNumber}
                                                           key={index}/>
                            })
                        }
                    </div>
                </Accordion.Collapse>
            </Accordion>
        </div>
    )

}   