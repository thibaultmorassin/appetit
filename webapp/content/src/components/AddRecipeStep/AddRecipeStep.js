import React, {useState} from "react";
import './AddRecipeStep.css';
import plusIcon from "../../images/icons/plus.svg";
import Button from "../Button/Button";

export default function AddRecipeStep(props) {
    const [stepIndex, setStepIndex] = useState(1);
    const [recipeStepContent, setRecipeStepContent] = useState("");
    const [emptyFieldAlert, setEmptyFieldAlert] = useState(false);

    const addRecipeStepContent = (e) => {
        if (e)
            e.preventDefault();
        if (recipeStepContent !== "") {
            props.handleNewElement([...props.value, {
                index: stepIndex,
                content: recipeStepContent
            }])
            setRecipeStepContent("");
            setStepIndex(props.value.length + 2);
        } else {
            setEmptyFieldAlert(true);
        }
    };

    return (
        <>
            <div className="add-recipe-step-item" onClick={props.handleOnClick}>
                <div className="recipe-step-item-index-container">
                    <div className={"recipe-step-item-index" + (props.isActive ? "-active" : "")}>
                        <div className="m-auto">
                            {stepIndex}
                        </div>
                    </div>
                </div>
                <div className="recipe-step-item-label m-0">
                    <textarea
                        className={"add-ingredients-textarea" + (recipeStepContent ? " add-ingredients-input-not-empty" : "")
                        + (emptyFieldAlert && !recipeStepContent ? " add-ingredients-input-empty-alert" : "")}
                        id={"add-recipe-step-input"}
                        rows={2}
                        placeholder={"Entrez l'étape " + stepIndex + " de la recette..."}
                        onKeyDown={(e) => e.keyCode === 13 ? addRecipeStepContent(e) : null}
                        value={recipeStepContent}
                        onChange={(event) => setRecipeStepContent(event.target.value)}/>
                </div>
            </div>
            {
                props.required && !props.value.length &&
                <p className="apt-input-alert text-center">Veuillez ajouter des étapes à la recette.</p>
            }
            <div className="py-3 text-center">
                <Button title={"Ajouter une étape"}
                        iconLeft={plusIcon}
                        variant={"secondary"}
                        handleOnClick={() => addRecipeStepContent()}
                />
            </div>
        </>
    )

}