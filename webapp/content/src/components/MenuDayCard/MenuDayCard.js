import React, {useState} from "react";
import './MenuDayCard.css';
import {Col} from "react-bootstrap";
import * as Icon from 'react-feather';

import {getDateLabel} from "../../functions/date-function";
import MenuDayCardRecipe from "../MenuDayCardRecipe/MenuDayCardRecipe";
import Popup from "../Popup/Popup";
import SelectGroup from "../SelectGroup/SelectGroup";
import SelectGroupItem from "../SelectGroupItem/SelectGroupItem";
import GuestNumberPicker from "../GuestNumberPicker/GuestNumberPicker";
import InputGroup from "../InputGroup/InputGroup";
import apiMenu from "../../api/api-menu";
import {cryptData, decryptData} from "../../functions/users-infos";

export default function MenuDayCard(props) {
    const [addMenuPopup, setAddMenuPopup] = useState(false);
    const [addRecipeTitle, setAddRecipeTitle] = useState("");
    const [addRecipeMoment, setAddRecipeMoment] = useState("");
    const [addRecipeGuestNumber, setAddRecipeGuestNumber] = useState(2);
    const [emptyFieldAlert] = useState(false);

    function addMenu() {
        let menu = {
            day: props.day,
            peopleNumber: addRecipeGuestNumber,
            moment: addRecipeMoment,
            recipeId: "",
            recipeTitle: addRecipeTitle,
        }
        apiMenu.newMenu(cryptData(menu)).then(result => {
            if (result.status === 200) {
                props.handleWeekMenu(decryptData(result.data.weekMenu).findRecipe);
                setAddMenuPopup(false);
            }
        });
    }

    props.recipesOfTheDay.sort((a, b) => (a.moment > b.moment) ? 1 : -1);

    const popupAddMenuContent =
        <div className="popup-edit-menu">
            <h5 className="text-center font-weight-bold">Menu de <span
                className="text-lowercase">{getDateLabel(props.day)}</span> :</h5>
            <hr/>
            <InputGroup controlId={"input-menu-add-recipe"}
                        label={"Titre de la recette"}
                        required={emptyFieldAlert}
                        value={addRecipeTitle}
                        transparent={true}
                        handleOnChange={setAddRecipeTitle}/>
            <hr/>
            <SelectGroup label={"Moment :"} value={addRecipeMoment} centerItems={true}>
                <SelectGroupItem label={"MIDI"}
                                 className="mx-2 px-5"
                                 handleOnClick={() => setAddRecipeMoment("MIDI")}
                                 isSelected={addRecipeMoment === "MIDI"}/>
                <SelectGroupItem label={"SOIR"}
                                 className="mx-2 px-5"
                                 handleOnClick={() => setAddRecipeMoment("SOIR")}
                                 isSelected={addRecipeMoment === "SOIR"}/>
            </SelectGroup>
            <hr className="mt-4"/>
            <div className="mt-3">
                <p>Nombre de personnes :</p>
                <div className="position-relative w-75 m-auto d-flex">
                    <p className="guest-number-label w-100">
                        <span
                            className="font-weight-bold">{addRecipeGuestNumber}</span> personne{addRecipeGuestNumber === 1 ? "" : "s"}
                    </p>
                    <GuestNumberPicker guestNumber={addRecipeGuestNumber} setGuestNumber={setAddRecipeGuestNumber}/>
                </div>
            </div>
            <hr className="mb-4 mt-3"/>
        </div>;

    props.recipesOfTheDay.sort((a, b) => (a.moment > b.moment) ? 1 : -1);

    return (
        <>
            <Col lg={3} xs={props.menuDisplay === "list" ? 12 : 6}
                 className={props.menuDisplay === "list" ? "" : "px-2"}>
                <div className={props.activeDay ? "menu-day-card menu-day-card-active" : "menu-day-card"}>
                    <div className="text-center">
                        {
                            props.activeDay &&
                            <Icon.Bookmark fill={"#1e1e1b"} color={"#1e1e1b"} className="menu-day-card-label-active"
                                           size={32}/>
                        }
                        <Icon.RefreshCcw className="menu-day-card-reset-button"
                                         size={props.menuDisplay === "list" ? 20 : 14}
                                         onClick={props.handleOnClick}/>
                        <h4 className={"menu-day-card-title" + (props.activeDay ? " font-weight-bold" : "")}>{getDateLabel(props.day)}</h4>
                        <h6 className="menu-day-card-subtitle">{props.date}</h6>
                    </div>
                    <div className="menu-day-card-body apt-scroll">
                        {
                            props.recipesOfTheDay.map((menu, index) => {
                                    return <MenuDayCardRecipe key={index} title={menu.title}
                                                              peopleNumber={menu.peopleNumber}
                                                              day={getDateLabel(props.day)} id={menu.menuId}
                                                              moment={menu.moment} added={!menu.description}
                                                              handleWeekMenu={props.handleWeekMenu}
                                    />
                                }
                            )
                        }
                    </div>
                    <div className="menu-day-card-add-button-container">
                        <Icon.PlusCircle className="menu-day-card-add-button" size={24} color="#1E1E1B"
                                         onClick={() => setAddMenuPopup(true)}/>
                    </div>
                </div>
            </Col>
            {
                addMenuPopup &&
                <Popup closeButton={true} closeButtonText={"Valider"} onClose={() => addMenu()}>
                    <div className="popup-add-recipe">
                        {popupAddMenuContent}
                    </div>
                </Popup>
            }
        </>
    );
}

