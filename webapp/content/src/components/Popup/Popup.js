import React, {useContext, useEffect, useState} from "react";
import './Popup.css'
import GlobalState from "../../context/GlobalState";
import * as Icon from 'react-feather';
import Draggable from 'react-draggable';
import Button from "../Button/Button";
import check from "../../images/icons/check.svg";

export default function Popup(props) {
    const globalState = useContext(GlobalState);
    const [hidePopupAnimation, setHidePopupAnimation] = useState(false);

    useEffect(() => {
        document.body.classList.add('popup-open');
        return function cleanup() {
            document.body.classList.remove('popup-open');
        };
    });

    const handleOnDrag = (e, ui) => {
        if (ui.lastY >= 80) {
            setHidePopupAnimation(true);
            setTimeout(function () {
                props.onClose();
            }, 400);
        }
    };

    const handleOnClose = () => {
        setHidePopupAnimation(true);
        setTimeout(function () {
            props.onClose();
        }, 400);
    };

    return (
        <>
            {
                globalState.isMobile &&
                <>
                    <div className="popup-mask"/>
                    <div
                        className={"animate__animated animate__slideInUp popup-mask bg-transparent" + (hidePopupAnimation ? " animate__slideOutDown" : "")}>
                        <Draggable
                            allowAnyClick={true}
                            handle={".popup-mobile-handler"}
                            bounds={{top: 0}}
                            onDrag={handleOnDrag}
                            axis="y">
                            <div className="popup-container-mobile">
                                <div className="popup-mobile-handler">
                                    <div className="popup-mobile-close-btn"/>
                                </div>
                                {
                                    props.children
                                }
                                <div className="my-3 text-center">
                                    {
                                        props.closeButton &&
                                        <Button title={props.closeButtonText ? props.closeButtonText : "FERMER"}
                                                iconRight={check}
                                                variant={"secondary"}
                                                handleOnClick={handleOnClose}
                                        />
                                    }
                                </div>
                            </div>
                        </Draggable>
                    </div>
                </>
            }
            {
                !globalState.isMobile &&
                <>
                    <div className="popup-mask"/>
                    <div
                        className={"animate__animated animate__slideInDown animate__faster popup-mask bg-transparent  align-item-center"
                        + (hidePopupAnimation ? " animate__slideOutUp" : "")}>
                        <div className="popup-container">
                            <div className="popup-close-btn" onClick={handleOnClose}>
                                <Icon.XCircle/>
                            </div>
                            <div className="popup-content">
                                {
                                    props.children
                                }
                                <div className="mt-3 mb-4 text-center">
                                    {
                                        props.closeButton &&
                                        <Button title={props.closeButtonText ? props.closeButtonText : "FERMER"}
                                                iconRight={check}
                                                variant={"secondary"}
                                                handleOnClick={handleOnClose}
                                        />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            }
        </>
    )
};
