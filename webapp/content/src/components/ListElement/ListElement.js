import React from "react";
import './ListElement.css';
import trueCheckBox from "../../images/icons/checkbox-true.svg"
import falseCheckbox from "../../images/icons/checkbox-false.svg"

export default function ListElement(props) {
    return (
        <div className="list-element-container">
            <img className="list-element-checkmark" src={props.checked ? trueCheckBox : falseCheckbox}
                 onClick={props.handleDelete}
                 alt={"✅"}/>
            <p className="list-element">{props.quantity} {props.unity} {(!props.quantity && !props.unity) ? "" : " - "}{props.title}</p>
        </div>
    );
}

