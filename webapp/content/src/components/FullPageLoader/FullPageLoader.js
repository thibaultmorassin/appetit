import React, {useEffect} from "react";
import './FullPageLoader.css';
import SyncLoader from "react-spinners/SyncLoader";

export default function FullPageLoader() {

    useEffect(() => {
        document.body.classList.add('popup-open');
        return function cleanup() {
            document.body.classList.remove('popup-open');
        };
    });

    return (
        <div className="full-page-loader">
            <div className="m-auto">
                <SyncLoader color={"#1E1E1B"} size={16}/>
            </div>
        </div>
    );
}