import React from "react";
import './PictureBand.css';

export default function PictureBand(props) {
    const backgroundPicture = {
        backgroundImage: `url(${props.backgroundPicture})`,
        height: props.height
    };

    return (
        <div className="picture-band" style={backgroundPicture}>
            <div className="picture-band-content">
                {props.children}
            </div>
        </div>
    );
}

