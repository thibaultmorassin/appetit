import React, {useContext, useState} from "react";
import './MenuDayCardRecipe.css';
import * as Icon from 'react-feather';
import Popup from "../Popup/Popup";
import SelectGroupItem from "../SelectGroupItem/SelectGroupItem";
import SelectGroup from "../SelectGroup/SelectGroup";
import GuestNumberPicker from "../GuestNumberPicker/GuestNumberPicker";
import InputGroup from "../InputGroup/InputGroup";
import apiMenu from "../../api/api-menu";
import {cryptData, decryptData} from "../../functions/users-infos";
import Button from "../Button/Button";
import deleteIcon from "../../images/icons/x.svg";
import check from "../../images/icons/check.svg";
import {Row, Col} from "react-bootstrap";
import GlobalState from "../../context/GlobalState";

export default function MenuDayCardRecipe(props) {
    const [editMenuPopup, setEditMenuPopup] = useState(false);
    const [editRecipeMoment, setEditRecipeMoment] = useState(props.moment);
    const [editRecipeTitle, setEditRecipeTitle] = useState(props.title);
    const [editRecipeGuestNumber, setEditRecipeGuestNumber] = useState(props.peopleNumber);
    const globalState = useContext(GlobalState);

    function updateMenu() {
        let menu;
        if (props.added) {
            menu = {
                _id: props.id,
                peopleNumber: editRecipeGuestNumber,
                moment: editRecipeMoment,
                recipeTitle: editRecipeTitle,
            }
        } else {
            menu = {
                _id: props.id,
                peopleNumber: editRecipeGuestNumber,
                moment: editRecipeMoment,
            }
        }
        apiMenu.updateMenu(cryptData(menu)).then(result => {
            if (result.status === 200) {
                props.handleWeekMenu(decryptData(result.data.weekMenu).findRecipe);
                setEditMenuPopup(false);
            }
        });
    }

    const deleteDayRecipe = () => {
        apiMenu.deleteMenu(cryptData(props.id)).then(result => {
            if (result.status === 200) {
                props.handleWeekMenu(decryptData(result.data.weekMenu).findRecipe);
                setEditMenuPopup(false);
            }
        });
    }

    const popupEditMenuContent =
        <div className="popup-edit-menu">
            <h5 className="text-center font-weight-bold">Menu du <span
                className="text-lowercase">{props.moment}</span> de <span
                className="text-uppercase">{props.day}</span> :</h5>
            <h5 className="text-center mb-3">{props.title}</h5>
            <hr/>
            <p>Que souhaitez-vous modifier ?</p>
            {props.added && <>
                <InputGroup controlId={"input-menu-add-recipe"}
                            label={"Titre de la recette"}
                            value={editRecipeTitle}
                            transparent={true}
                            handleOnChange={setEditRecipeTitle}/>
            </>
            }
            <SelectGroup label={"Moment :"} value={editRecipeMoment} centerItems={true}>
                <SelectGroupItem label={"MIDI"}
                                 className="mx-2 px-5"
                                 handleOnClick={() => setEditRecipeMoment("MIDI")}
                                 isSelected={editRecipeMoment === "MIDI"}/>
                <SelectGroupItem label={"SOIR"}
                                 className="mx-2 px-5"
                                 handleOnClick={() => setEditRecipeMoment("SOIR")}
                                 isSelected={editRecipeMoment === "SOIR"}/>
            </SelectGroup>
            <div className="mt-3">
                <p>Nombre de personnes :</p>
                <div className="position-relative w-75 m-auto d-flex">
                    <p className="guest-number-label mx-5">
                        <span
                            className="font-weight-bold">{editRecipeGuestNumber}</span> personne{editRecipeGuestNumber === 1 ? "" : "s"}
                    </p>
                    <GuestNumberPicker guestNumber={editRecipeGuestNumber} setGuestNumber={setEditRecipeGuestNumber}/>
                </div>
            </div>
            <hr className="my-4"/>
            <Row className="text-center">
                <Col md={6} className={globalState.isMobile ? "my-3" : ""}>
                    <Button title={"Supprimer"}
                            width={globalState.isMobile ? "100%" : ""}
                            iconRight={deleteIcon}
                            variant={"black"}
                            handleOnClick={deleteDayRecipe}
                    />
                </Col>
                <Col md={6}>
                    <Button title={"Valider"}
                            width={globalState.isMobile ? "100%" : ""}
                            iconRight={check}
                            variant={"secondary"}
                            handleOnClick={updateMenu}
                    />
                </Col>
            </Row>
        </div>

    return (
        <>
            <div className="d-flex mb-2 position-relative">
                <div className={"menu-day-card-recipe-span span-" + props.moment}/>
                <div className="menu-day-card-recipe">
                    <h6 className="menu-day-card-recipe-title">{props.title}</h6>
                    <Icon.Edit className="menu-day-card-recipe-edit" size={16} onClick={() => setEditMenuPopup(true)}/>
                    <div className="d-flex">
                        <p className="menu-day-card-recipe-people-number">{props.peopleNumber} personne{props.peopleNumber > 1 ? "s" : ""} </p>
                        <p className="menu-day-card-recipe-moment">{props.moment}</p>
                    </div>
                </div>
            </div>
            {
                editMenuPopup &&
                <Popup closeButton={false} closeButtonText={"Valider"} onClose={() => setEditMenuPopup(false)}>
                    <div className="popup-add-recipe">
                        {popupEditMenuContent}
                    </div>
                </Popup>
            }
        </>
    );
}

