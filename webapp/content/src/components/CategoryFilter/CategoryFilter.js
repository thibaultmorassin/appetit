import React from "react";
import './CategoryFilter.css';

export default function CategoryFilter(props) {

    return (
        <div
            className={"category-filter" + (props.active ? " category-filter-active" : "") + (props.disabled ? " category-filter-disabled" : "")}
            onClick={props.handleOnClick}>
            {props.title}
        </div>
    );
}   