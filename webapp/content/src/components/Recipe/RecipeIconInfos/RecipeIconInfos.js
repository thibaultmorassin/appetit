import React from "react";
import './RecipeIconInfos.css';

export default function RecipeIconInfos(props) {
    const timeConverter = (timeInt) => {
        var timeString;
        var hours = Math.floor(timeInt / 60);
        var minutes = timeInt % 60;

        minutes = minutes + (minutes.toString().length === 1 ? "0" : "");
        timeString = (hours ? (hours + "h") : "") + minutes + (hours ? "" : "min");

        return timeString;
    };

    const priceConverter = (priceInt) => {
        var priceString = "";
        var priceStaying = "";
        for (let i = 0; i < priceInt; i++) {
            priceString += "€";
        }
        for (let i = priceInt; i < 3; i++) {
            priceStaying += "€";
        }
        return <>
            <span className="font-weight-bold">{priceString}</span>
            <span className="light-text">{priceStaying}</span>
        </>;
    };

    const difficultyConverter = (difficultyInt) => {
        var full = "";
        var empty = "";
        for (let i = 0; i < difficultyInt; i++) {
            full += "★";
        }
        for (let i = difficultyInt; i < 3; i++) {
            empty += "☆";
        }
        return <>
            <span className="font-weight-bold">{full}</span>
            <span className="light-text">{empty}</span>
        </>;
    };

    return (
        <div className={"recipe-icons-container" + (props.recipePage ? " recipe-page-icons" : "")}>
            <div className={"recipe-icon" + (props.small ? " recipe-icon-small" : "")}>
                {timeConverter(props.recipe.duration)}
            </div>
            <div className={"recipe-icon" + (props.small ? " recipe-icon-small" : "")}>
                {priceConverter(props.recipe.price)}
            </div>
            <div className={"recipe-icon" + (props.small ? " recipe-icon-small" : "")}>
                {difficultyConverter(props.recipe.difficulty)}
            </div>
        </div>
    );
}

