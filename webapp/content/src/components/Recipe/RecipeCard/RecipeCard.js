import React, {useContext} from "react";
import './RecipeCard.css';
import Col from "react-bootstrap/cjs/Col";
import recipe from "../../../images/recipe.png";
import RecipeIconInfos from "../RecipeIconInfos/RecipeIconInfos";
import Button from "../../Button/Button";
import arrowRight from "../../../images/icons/arrow-right.svg";
import GlobalState from "../../../context/GlobalState";
import {cryptData} from "../../../functions/users-infos";

export default function RecipeCard(props) {
    const globalState = useContext(GlobalState);

    const goToRecipe = () => {
        window.location = "/recette/" + props.recipe.title;
        localStorage.setItem("recipe",cryptData(props.recipe));
    }

    return (
        <Col md={3} xs={props.recipeDisplay === "list" ? 12 : 6} className="recipe-card-col">
            <div className="animate__animated animate__fadeIn recipe-card" onClick={() => goToRecipe()}>
                <div className="recipe-card-header">
                    <img className="recipe-card-picture" draggable={false}
                         src={props.recipe.picture ? props.recipe.picture : recipe} alt={props.recipe.title}/>
                    <div className="recipe-card-picture-gradient"/>
                </div>
                <div className="recipe-card-body">
                    <p className="recipe-card-title">{props.recipe.title}</p>
                    {
                        !globalState.isMobile &&
                        <p className="recipe-card-description">{props.recipe.description}</p>
                    }
                    <RecipeIconInfos recipe={props.recipe} small={true}/>
                    {
                        !globalState.isMobile &&
                        <div className="mt-2 text-center">
                            <Button title={"VOIR LA RECETTE"}
                                    iconRight={arrowRight}
                                    variant={"primary"}
                                    handleOnClick={() => goToRecipe()}
                            />
                        </div>
                    }
                </div>
            </div>
        </Col>
    );
}