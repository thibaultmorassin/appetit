import React from "react";
import './IngredientListItem.css';

export default function IngredientListItem(props) {
    const handleGuestNumber = (quantity) => {
        return quantity * props.guestNumber;
    };

    return (
        <div className="ingredients-list-item">
            <div className="ingredient-list-label">
                {props.ingredient.label}
            </div>
            <div className="ingredient-list-quantiy">
                {handleGuestNumber(props.ingredient.quantity)} {props.ingredient.unity}
            </div>
        </div>
    )

}   