import React from "react";
import './SelectGroupItem.css';

export default function SelectGroupItem(props) {

    return (
        <div
            className={"apt-select-item" + (props.isSelected ? " apt-select-item-selected" : "") + " " + props.className}
            onClick={props.handleOnClick}>
            {props.label}
        </div>

    );
}

