import React, {useState} from "react";
import '../AddIngredientInputGroup/AddIngredientInputGroup.css';
import plusIcon from "../../images/icons/plus.svg";
import Button from "../Button/Button";

export default function AddTagInputGroup(props) {
    const [tagLabel, setTagLabel] = useState("");
    const [emptyFieldAlert, setEmptyFieldAlert] = useState(false);

    const addTagToList = () => {
            props.handleNewElement([...props.value,
                tagLabel,
            ])
            setTagLabel("");
            document.getElementById("add-ingredients-input-label").focus()
    };

    return (
        <>
            <div className="add-ingredients-input-container">
                <div className="flex-6">
                    <input
                        className={"add-ingredients-input" + (tagLabel ? " add-ingredients-input-not-empty" : "")
                        + (emptyFieldAlert && !tagLabel ? " add-ingredients-input-empty-alert" : "")}
                        type={"text"}
                        id={"add-ingredients-input-label"}
                        onKeyDown={(e) => e.keyCode === 13 ? addTagToList() : null}
                        value={tagLabel}
                        onChange={(event) => setTagLabel(event.target.value)}/>
                </div>

            </div>
            <div className="py-3 text-center">
                <Button title={"Ajouter un tag"}
                        iconLeft={plusIcon}
                        variant={"secondary"}
                        handleOnClick={() => addTagToList()}
                />
            </div>
        </>
    )

}