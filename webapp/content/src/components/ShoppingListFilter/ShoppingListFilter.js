import React from "react";
import './ShoppingListFilter.css';

export default function ShoppingListFilter(props) {
    return (
        <div className={"shopping-list-filter" + (props.active ? " shopping-list-filter-active" : "")}
             onClick={props.handleOnClick}>
            {props.title}
        </div>
    );
}

