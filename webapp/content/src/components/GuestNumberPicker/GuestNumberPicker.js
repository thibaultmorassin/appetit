import React from "react";
import './GuestNumberPicker.css';

export default function GuestNumberPicker(props) {

    return (
        <div className="guest-number-picker">
            <div className="guest-number-picker-button guest-less"
                 onClick={() => {
                     if (props.guestNumber !== 1)
                         props.setGuestNumber(props.guestNumber - 1)
                 }}>
                -
            </div>
            <div className="guest-number-picker-button guest-plus"
                 onClick={() => props.setGuestNumber(props.guestNumber + 1)}>
                +
            </div>
        </div>
    )
}