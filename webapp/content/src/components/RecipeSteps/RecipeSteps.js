import React, {useState} from "react";
import './RecipeSteps.css';
import RecipeStepsItem from "../RecipeStepsItem/RecipeStepsItem";

export default function RecipeSteps(props) {
    const [stepIndexActive, setStepIndexActive] = useState(0);

    return (
        <div className="apt-container-light pb-3 mt-3">
            <h6 className="steps-title">Étapes</h6>
            {
                props.steps !== undefined &&
                props.steps.map((step, index) => {
                        return <RecipeStepsItem step={step}
                                                handleOnClick={() => setStepIndexActive(index)}
                                                isActive={stepIndexActive === (index)}
                                                stepIndex={index + 1}
                                                isLastStep={index === props.steps.length - 1}
                                                key={index}/>
                    }
                )
            }
        </div>
    )
}