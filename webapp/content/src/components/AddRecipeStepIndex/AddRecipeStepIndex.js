import React from "react";
import './AddRecipeStepIndex.css';

export default function AddRecipeStepIndex(props) {

    return (
        <div className="add-recipe-step-index-container">
            <div
                className={"add-recipe-step-index" + (props.stepIndex === 1 ? "-active" : "") + (props.stepIndex > 1 ? "-done" : "")}
            onClick={() => props.handleOnClick(1)}>
                <div className="m-auto">
                    1
                </div>
            </div>
            <div className={"add-recipe-step-index-separator" + (props.stepIndex > 1 ? "-done" : "")}/>
            <div
                className={"add-recipe-step-index" + (props.stepIndex === 2 ? "-active" : "") + (props.stepIndex > 2 ? "-done" : "")}
            onClick={() => props.handleOnClick(2)}>
                <div className="m-auto">
                    2
                </div>
            </div>
            <div className={"add-recipe-step-index-separator" + (props.stepIndex > 2 ? "-done" : "")}/>
            <div
                className={"add-recipe-step-index" + (props.stepIndex === 3 ? "-active" : "") + (props.stepIndex > 3 ? "-done" : "")}
            onClick={() => props.handleOnClick(3)}>
                <div className="m-auto">
                    3
                </div>
            </div>
        </div>
    );
}

