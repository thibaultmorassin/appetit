import React from "react";
import './SelectGroup.css';

export default function SelectGroup(props) {

    return (
        <div className="apt-select-group">
            <p className="apt-select-label">
                {props.label}
            </p>
            {
                props.required && !props.value &&
                <p className="apt-select-group-alert">Veuillez remplir ce champ.</p>
            }
            <div className={"d-flex" + (props.centerItems ? " w-fit-content mx-auto" : "")}>
                {props.children}
            </div>
        </div>

    );
}

