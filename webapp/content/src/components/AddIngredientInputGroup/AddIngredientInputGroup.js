import React, {useState} from "react";
import './AddIngredientInputGroup.css';
import plusIcon from "../../images/icons/plus.svg";
import Button from "../Button/Button";

export default function AddIngredientInputGroup(props) {
    const [ingredientLabel, setIngredientLabel] = useState("");
    const [ingredientQuantity, setIngredientQuantity] = useState("");
    const [ingredientUnity, setIngredientUnity] = useState("");
    const [emptyFieldAlert, setEmptyFieldAlert] = useState(false);

    const addIngredientToList = () => {
        if (ingredientLabel !== "" && ingredientQuantity >= 0) {
            props.handleNewElement([...props.value, {
                label: ingredientLabel,
                quantity: Number.parseFloat(ingredientQuantity / props.guestNumber).toPrecision(2),
                unity: ingredientUnity
            }])
            setIngredientLabel("");
            setIngredientQuantity("");
            setIngredientUnity("");
            document.getElementById("add-ingredients-input-label").focus()
        } else {
            setEmptyFieldAlert(true)
        }
    };

    return (
        <>
            <div className="add-ingredients-input-container">
                <div className="flex-6">
                    <input
                        className={"add-ingredients-input" + (ingredientLabel ? " add-ingredients-input-not-empty" : "")
                        + (emptyFieldAlert && !ingredientLabel ? " add-ingredients-input-empty-alert" : "")}
                        type={"text"}
                        id={"add-ingredients-input-label"}
                        onKeyDown={(e) => e.keyCode === 13 ? addIngredientToList() : null}
                        value={ingredientLabel}
                        onChange={(event) => setIngredientLabel(event.target.value)}/>
                </div>

                <div className="flex-3 pl-3">
                    <input
                        className={"add-ingredients-input text-right" + (ingredientQuantity ? " add-ingredients-input-not-empty" : "")
                        + (emptyFieldAlert && !ingredientQuantity ? " add-ingredients-input-empty-alert" : "")}
                        type={"number"}
                        onKeyDown={(e) => e.keyCode === 13 ? addIngredientToList() : null}
                        value={ingredientQuantity}
                        onChange={(event) => setIngredientQuantity(event.target.value)}/>
                </div>

                <div className="flex-2 pl-3">
                    <input
                        className={"add-ingredients-input text-right" + (emptyFieldAlert ? " add-ingredients-input-not-empty" : "")
                        + (emptyFieldAlert && !ingredientUnity ? " add-ingredients-input-empty-alert" : "")}
                        type={"text"}
                        onKeyDown={(e) => e.keyCode === 13 ? addIngredientToList() : null}
                        value={ingredientUnity}
                        onChange={(event) => setIngredientUnity(event.target.value)}/>
                </div>
            </div>
            {
                props.required && !props.value.length &&
                <p className="apt-input-alert text-center">Veuillez ajouter des ingrédients.</p>
            }
            <div className="py-3 text-center">
                <Button title={"Ajouter un ingrédient"}
                        iconLeft={plusIcon}
                        variant={"secondary"}
                        handleOnClick={() => addIngredientToList()}
                />
            </div>
        </>
    )

}