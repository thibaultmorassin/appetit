const mongoose = require("mongoose");

const recipeSchema = mongoose.Schema(
    {
        title: String,
        category: String,
        tag:[String],
        description: String,
        picture: String,
        difficulty: Number,
        duration: Number,
        price: Number,
        favoris: Boolean,
        ingredients: [{
            quantity : String,
            unity : String,
            label : String,
        }],
        step : [{
            content : String
        }]
    },
);

module.exports = mongoose.model("Recipe", recipeSchema);
