const mongoose = require("mongoose");

const listSchema = mongoose.Schema(
    {
        quantity : String,
        unity : String,
        label : String,
        type: String,
    },
);

module.exports = mongoose.model("List", listSchema);
