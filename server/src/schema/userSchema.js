const mongoose = require("mongoose");

const userSchema = mongoose.Schema(
    {
        email: String,
        password: String,
        pseudo: String
    },
);

module.exports = mongoose.model("User", userSchema);
