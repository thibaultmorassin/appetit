const mongoose = require("mongoose");

const menuSchema = mongoose.Schema(
    {
        day: Number,
        peopleNumber : Number,
        moment: String,
        recipeId: String,
        recipeTitle: String,
    },
);

module.exports = mongoose.model("Menu", menuSchema);
