// ------ Modules ------ //
const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
require('dotenv').config();
const mongoose = require('mongoose');

// ------ Database ------ //
mongoose.connect(
    `mongodb+srv://${process.env.MONGO_LOGIN}:${process.env.MONGO_PASSWORD}@cluster0.shhfb.mongodb.net/${process.env.MONGO_DB_NAME}?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to MongoDB");
    })
    .catch(err => {
            console.error(err);
        }
    );

// ------ CORS configuration ------ //
app.use(cors());

// ------ Body Parser ------ //
//application/x-www-form-urlencoded parser
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
//application/json parser
app.use(bodyParser.json({ limit: '10mb' }));

// ------ Routers ------ //
const router = express.Router();
app.use("/users", router);
require(__dirname + "/controllers/users/userController")(router);
app.use("/menu", router);
require(__dirname + "/controllers/menu/menuController")(router);
app.use("/recipe", router);
require(__dirname + "/controllers/recipe/recipeController")(router);
app.use("/list", router);
require(__dirname + "/controllers/list/listController")(router);

// ------ Listening Port ------ //
const port = 8080;
app.listen(port);
console.log("Listening on port", port);
