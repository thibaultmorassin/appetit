// ----- /user functions ----- //
const ejs = require("ejs");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require("../../schema/userSchema.js");
const crypto = require('crypto');

/**
 * GET request to login an user
 * @param req
 * @param res
 * @returns {promise}
 */
async function loginUser(req, res) {
    const findUser = await User.findOne({pseudo: req.query.pseudo});
    if (!findUser) {
        return res.status(202).json({
            text: "User not found"
        });
    } else {
        const token = jwt.sign(
            {findUser},
            process.env.SECRET_TOKEN
        );
        return res.status(200).json({
            text: "Success",
            token: token
        });
    }
}

function checkToken(req, res) {
    return true;
}

exports.checkToken = checkToken;
exports.loginUser = loginUser;




