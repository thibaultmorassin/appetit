// Definition of Users routes

const user = require('./userFunctions.js');


module.exports = function (app) {
    // ~/users/loginUser
    app.get('/loginUser', user.loginUser);
};
