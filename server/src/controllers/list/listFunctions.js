// ----- /list functions ----- //
const ejs = require("ejs");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const List = require("../../schema/listSchema.js");
const crypto = require('crypto');

const UserFunctions = require("../../controllers/users/userFunctions");

/**
 * POST request to add a list
 * @param req
 * @param res
 * @returns {promise}
 */
async function newList(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        try {
            const decoded = jwt.verify(req.body.params.list, process.env.SECRET_TOKEN).data;
            const new_list = new List({
                quantity: decoded.quantity,
                unity: decoded.unity,
                label: decoded.label,
                type: decoded.type,
            });
            new_list.save();
            getList(req, res);
        } catch (err) {
            return res.status(202).json({
                text: "Failed to decode list"
            });
        }
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * GET request to get list
 * @param req
 * @param res
 * @returns {promise}
 */
async function getList(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        let findList = await List.find();
        return res.status(200).json({
            text: "List found",
            list: jwt.sign(
                {findList},
                process.env.SECRET_TOKEN
            ),
        });
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * GET request to get filtered list
 * @param req
 * @param res
 * @returns {promise}
 */
async function filterList(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        const type = req.query.type;
        let findList;

        if (type === "all") {
            findList = await List.find();
        } else {
            findList = await List.find({type: type}).select('quantity unity label type');
        }
        return res.status(200).json({
            text: "List found",
            list: jwt.sign(
                {findList},
                process.env.SECRET_TOKEN
            ),
        })
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * GET request to delete item
 * @param req
 * @param res
 * @returns {promise}
 */
async function deleteItem(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        const itemId = req.query.itemId;
        await List.findByIdAndDelete(itemId);
        const findList = await List.find();

        return res.status(200).json({
            text: "List found",
            list: jwt.sign(
                {findList},
                process.env.SECRET_TOKEN
            ),
        })
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * GET request to delete whole list
 * @param req
 * @param res
 * @returns {promise}
 */
async function deleteList(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        await List.deleteMany();
        const findList = await List.find();

        return res.status(200).json({
            text: "List found",
            list: jwt.sign(
                {findList},
                process.env.SECRET_TOKEN
            ),
        })
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

exports.newList = newList;
exports.getList = getList;
exports.filterList = filterList;
exports.deleteItem = deleteItem;
exports.deleteList = deleteList;




