// Definition of List routes

const list = require('./listFunctions.js');


module.exports = function (app) {
    // ~/list/newList
    app.post('/newList', list.newList);
    // ~/list/newList
    app.get('/getList', list.getList);
    // ~/list/filterList
    app.get('/filterList', list.filterList);
    // ~/list/deleteItem
    app.get('/deleteItem', list.deleteItem);
    // ~/list/deleteList
    app.get('/deleteList', list.deleteList);
};
