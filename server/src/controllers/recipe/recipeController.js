// Definition of Recipe routes

const recipe = require('./recipeFunctions.js');


module.exports = function (app) {
    // ~/recipe/newRecipe
    app.post('/newRecipe', recipe.newRecipe);
    // ~/recipe/changeFavorisRecipe
    app.post('/changeFavorisRecipe', recipe.changeFavorisRecipe);
    // ~/recipe/deleteRecipe
    app.post('/deleteRecipe', recipe.deleteRecipe);
    // ~/recipe/getFilteredRecipe
    app.get('/getFilteredRecipe', recipe.getFilteredRecipe);
    // ~/recipe/getFavorisRecipe
    app.get('/getFavorisRecipe', recipe.getFavorisRecipe);
    // ~/recipe/getRecipeById
    app.post('/getRecipeById', recipe.getRecipeById);
};
