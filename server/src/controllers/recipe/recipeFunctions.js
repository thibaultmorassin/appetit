// ----- /recipe functions ----- //
const ejs = require("ejs");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Recipe = require("../../schema/recipeSchema.js");
const crypto = require('crypto');

const UserFunctions = require("../../controllers/users/userFunctions");

/**
 * POST request to add a recipe
 * @param req
 * @param res
 * @returns {promise}
 */
async function newRecipe(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        try {
            const decoded = jwt.verify(req.body.params.recipe, process.env.SECRET_TOKEN).data;
            const new_recipe = new Recipe({
                title: decoded.title,
                category: decoded.category,
                tag: decoded.tag,
                description: decoded.description,
                picture: decoded.picture,
                difficulty: decoded.difficulty,
                duration: decoded.duration,
                price: decoded.price,
                favoris: decoded.favoris,
                ingredients: decoded.ingredients,
                step: decoded.step,
            });
            new_recipe.save();
            return res.status(200).json({
                text: "Recipe added",
                recipe: jwt.sign(
                    {new_recipe},
                    process.env.SECRET_TOKEN
                ),
            });
        } catch (err) {
            return res.status(202).json({
                text: "Failed to decode recipe"
            });
        }
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * POST request to change recipe's favoris field
 * @param req
 * @param res
 * @returns {promise}
 */
async function changeFavorisRecipe(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        try {
            const decoded = jwt.verify(req.body.params.recipe, process.env.SECRET_TOKEN).data;
            favoris = await Recipe.findOne({_id: decoded._id}).select('favoris');
            findRecipe = await Recipe.updateOne(
                {_id: decoded._id},
                {favoris: !favoris.favoris});
            getRecipeById(req, res)
        } catch (err) {
            return res.status(202).json({
                text: "Failed to decode recipe"
            });
        }
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * POST request to change recipe's favoris field
 * @param req
 * @param res
 * @returns {promise}
 */
async function deleteRecipe(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        try {
            const decoded = jwt.verify(req.body.params.recipe, process.env.SECRET_TOKEN).data;
            deletedRecipe = await Recipe.deleteOne({_id:decoded._id}).then(result => {
                    return res.status(200).json({
                        text: "Recipe deleted",
                    });
                }
            );
        } catch (err) {
            return res.status(202).json({
                text: "Failed to decode recipe"
            });
        }
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * GET request to get all recipe
 * @param req
 * @param res
 * @returns {promise}
 */
async function getFilteredRecipe(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        const category = req.query.category;
        const filters = req.query.filters;
        let findRecipe;
        if ((category === "" && filters === "") || (category === undefined && filters === "") || (category === "" && filters === undefined) || (category === undefined && filters === undefined)) {
            findRecipe = await Recipe.find({}).select('title description price duration difficulty picture');
        } else if (category !== "" && category !== undefined && (filters === undefined || filters === "")) {
            findRecipe = await Recipe.find({category: category}).select('title description price duration difficulty picture');
        } else if (filters !== "" && filters !== undefined && (category === undefined || category === "")) {
            findRecipe = await Recipe.find({tag: {$all: filters}}).select('title description price duration difficulty picture');
        } else if (category !== "" && category !== undefined && filters !== undefined && filters !== "") {
            findRecipe = await Recipe.find({
                category: category,
                tag: {$all: filters}
            }).select('title description price duration difficulty picture');
        }
        return res.status(200).json({
            text: "Recipe found",
            recipe: jwt.sign(
                {findRecipe},
                process.env.SECRET_TOKEN
            ),
        });
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * GET request to get all recipe
 * @param req
 * @param res
 * @returns {promise}
 */
async function getFavorisRecipe(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        const category = req.query.category;
        const filters = req.query.filters;
        let findRecipe;
        if ((category === "" && filters === "") || (category === undefined && filters === "") || (category === "" && filters === undefined) || (category === undefined && filters === undefined)) {
            findRecipe = await Recipe.find({favoris: true}).select('title description price duration difficulty picture');
        } else if (category !== "" && category !== undefined && (filters === undefined || filters === "")) {
            findRecipe = await Recipe.find({favoris: true,category: category}).select('title description price duration difficulty picture');
        } else if (filters !== "" && filters !== undefined && (category === undefined || category === "")) {
            findRecipe = await Recipe.find({favoris: true, tag: {$all: filters}}).select('title description price duration difficulty picture');
        } else if (category !== "" && category !== undefined && filters !== undefined && filters !== "") {
            findRecipe = await Recipe.find({
                favoris: true,
                category: category,
                tag: {$all: filters}
            }).select('title description price duration difficulty picture');
        }
        return res.status(200).json({
            text: "Recipe found",
            recipe: jwt.sign(
                {findRecipe},
                process.env.SECRET_TOKEN
            ),
        });
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * POST request to get a recipe by id
 * @param req
 * @param res
 * @returns {promise}
 */
async function getRecipeById(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        try {
            const decoded = jwt.verify(req.body.params.recipe, process.env.SECRET_TOKEN).data;
            findRecipe = await Recipe.findOne({_id: decoded._id});
            return res.status(200).json({
                text: "Recipe found",
                recipe: jwt.sign(
                    {findRecipe},
                    process.env.SECRET_TOKEN
                ),
            });
        } catch (err) {
            return res.status(202).json({
                text: "Failed to decode recipe"
            });
        }
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * POST request to change the recipe
 * @param req
 * @param res
 * @returns {promise}
 */
async function updateRecipe(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        try {
            const decoded = jwt.verify(req.body.params.recipe, process.env.SECRET_TOKEN).data;
            const recipe = await Recipe.updateOne({
                _id: decoded._id
                },
                {
                    title: decoded.title,
                    category: decoded.category,
                    tag: decoded.tag,
                    description: decoded.description,
                    picture: decoded.picture,
                    difficulty: decoded.difficulty,
                    duration: decoded.duration,
                    price: decoded.price,
                    favoris: decoded.favoris,
                    ingredients: decoded.ingredients,
                    step: decoded.step,
            });
            if (recipe.nModified === 1) {
                return res.status(200).json({
                    text: "Recipe updated",
                });
            } else {
                return res.status(201).json({
                    text: "User not found"
                });
            }
        } catch (err) {
            return res.status(202).json({
                text: "Failed to decode recipe"
            });
        }
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}


exports.newRecipe = newRecipe;
exports.changeFavorisRecipe = changeFavorisRecipe;
exports.deleteRecipe = deleteRecipe;
exports.getFilteredRecipe = getFilteredRecipe;
exports.getFavorisRecipe = getFavorisRecipe;
exports.getRecipeById = getRecipeById;
exports.updateRecipe = Recipe;



