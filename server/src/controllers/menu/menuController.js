// Definition of Menu routes

const menu = require('./menuFunctions.js');


module.exports = function (app) {
    // ~/menu/newMenu
    app.post('/newMenu', menu.newMenu);
    // ~/menu/updateMenu
    app.post('/updateMenu', menu.updateMenu);
    // ~/menu/getTodayMenu
    app.get('/getTodayMenu', menu.getTodayMenu);
    // ~/menu/getWeekMenu
    app.get('/getWeekMenu', menu.getWeekMenu);
    // ~/menu/deleteMenu
    app.post('/deleteMenu', menu.deleteMenu);
};
