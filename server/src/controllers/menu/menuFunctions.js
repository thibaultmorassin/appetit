// ----- /menu functions ----- //
const ejs = require("ejs");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Menu = require("../../schema/menuSchema.js");
const Recipe = require("../../schema/recipeSchema.js");
const crypto = require('crypto');

const UserFunctions = require("../../controllers/users/userFunctions");

/**
 * POST request to add a menu
 * @param req
 * @param res
 * @returns {promise}
 */
async function newMenu(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        try {
            const decoded = jwt.verify(req.body.params.menu, process.env.SECRET_TOKEN).data;
            const new_menu = new Menu({
                day: decoded.day,
                peopleNumber: decoded.peopleNumber,
                moment: decoded.moment,
                recipeId: decoded.recipeId,
                recipeTitle: decoded.recipeTitle,
            });
            new_menu.save();
            getWeekMenu(req, res);
        } catch (err) {
            return res.status(202).json({
                text: "Failed to decode menu"
            });
        }
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * POST request to update a menu
 * @param req
 * @param res
 * @returns {promise}
 */
async function updateMenu(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        try {
            const decoded = jwt.verify(req.body.params.menu, process.env.SECRET_TOKEN).data;
            findRecipe = await Menu.updateOne(
                {_id: decoded._id},
                decoded);
            if (findRecipe.nModified !== 0) {
                getWeekMenu(req, res);
            } else {
                return res.status(201).json({
                    text: "Menu not found",
                });
            }
        } catch (err) {
            return res.status(202).json({
                text: "Failed to decode menu"
            });
        }
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * GET request to get today's menu
 * @param req
 * @param res
 * @returns {promise}
 */
async function getTodayMenu(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        const today = new Date().getDay();
        let findMenu = await Menu.find({day: today});
        let findRecipe = [];
        for (menu of findMenu) {
            if (menu.recipeId !== "") {
                let request = {
                    _id: menu.recipeId,
                };
                recipe = await Recipe.findOne(request).select('title description price duration difficulty picture');
                recipe = JSON.parse(JSON.stringify(recipe));
                recipe.peopleNumber = menu.peopleNumber;
                recipe.moment = menu.moment;
                findRecipe.push(recipe);
            } else {
                recipe = {
                    title: menu.recipeTitle,
                    peopleNumber: menu.peopleNumber,
                    moment: menu.moment,
                }
                findRecipe.push(recipe);
            }
        }
        if (findRecipe.length !== 0) {
            return res.status(200).json({
                text: "Menu found",
                todayMenu: jwt.sign(
                    {findRecipe},
                    process.env.SECRET_TOKEN
                ),
            });
        } else {
            return res.status(202).json({
                text: "Menu empty",
            });
        }
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * GET request to get today's menu
 * @param req
 * @param res
 * @returns {promise}
 */
async function getWeekMenu(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        let findMenu = await Menu.find({});
        let findRecipe = [];
        for (menu of findMenu) {
            if (menu.recipeId !== "") {
                let request = {
                    _id: menu.recipeId,
                };
                recipe = await Recipe.findOne(request).select('title description');
                recipe = JSON.parse(JSON.stringify(recipe));
                recipe.peopleNumber = menu.peopleNumber;
                recipe.moment = menu.moment;
                recipe.day = menu.day;
                recipe.menuId = menu._id
                findRecipe.push(recipe);
            } else {
                recipe = {
                    menuId: menu._id,
                    title: menu.recipeTitle,
                    peopleNumber: menu.peopleNumber,
                    moment: menu.moment,
                    day: menu.day,
                }
                findRecipe.push(recipe);
            }
        }
        if (findRecipe.length !== 0) {
            return res.status(200).json({
                text: "Menu found",
                weekMenu: jwt.sign(
                    {findRecipe},
                    process.env.SECRET_TOKEN
                ),
            });
        } else {
            return res.status(202).json({
                text: "Menu empty",
            });
        }
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

/**
 * POST request to delete
 * @param req
 * @param res
 * @returns {promise}
 */
async function deleteMenu(req, res) {
    if (UserFunctions.checkToken(req, res)) {
        try {
            const decoded = jwt.verify(req.body.params.day, process.env.SECRET_TOKEN).data;
            if (decoded !== "") {
                console.log(decoded.length)
                if (decoded.length === undefined) {
                    await Menu.deleteMany({day: decoded}).then(getWeekMenu(req, res));
                } else {
                    await Menu.deleteMany({_id: decoded}).then(getWeekMenu(req, res));
                }
            } else {
                await Menu.deleteMany({}).then(getWeekMenu(req, res));
            }
        } catch (err) {
            return res.status(202).json({
                text: "Failed to decode menu"
            });
        }
    } else {
        return res.status(205).json({
            text: "Wrong Token"
        });
    }
}

exports.newMenu = newMenu;
exports.updateMenu = updateMenu;
exports.getTodayMenu = getTodayMenu;
exports.getWeekMenu = getWeekMenu;
exports.deleteMenu = deleteMenu;




