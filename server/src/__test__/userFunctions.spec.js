const axios = require('axios');
require('dotenv').config();

describe('Check login', () => {

    test('User exist', async () => {
        const res = await axios.get(
            `http://localhost:8080/users/loginUser`,
            {
                params: {
                    pseudo: "Appetit2021",
                }
            });
        expect(res.status).toBe(200);
    }, 30000);

    test('User does not exist', async () => {
        const res = await axios.get(
            `http://localhost:8080/users/loginUser`,
            {
                params: {
                    pseudo: "idontevenexist",
                }
            });
        expect(res.status).toBe(202);
    }, 30000);
});
